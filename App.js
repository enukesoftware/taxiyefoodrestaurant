/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, DeviceEventEmitter } from 'react-native';
import { createAppContainer, SwitchNavigator, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack'
import Home from './app/components/Home';
import Dashboard from './app/components/Dashboard'
import OrderDetail from './app/components/OrderDetail'
import OrderList from './app/components/OrderList'
import SearchOrder from './app/components/SearchOrder'
import AppDrawerNavigator from './app/components/Dashboard/SampleDashboard'
import NavigationService from './app/services/NavigationServices'
import AsyncStorage from '@react-native-community/async-storage'
import ForgotPassword from './app/components/ForgotPassword'
import SplashScreen from 'react-native-splash-screen'
import firebase from 'react-native-firebase'
import Constants from './app/constants/constants'
import DriverList from './app/components/DriverList'

const AppStack = createStackNavigator({
  Dashboard: {
    screen: Dashboard
  },
  OrderDetail: {
    screen: OrderDetail
  },
  OrderList: {
    screen: OrderList
  },
  SearchOrder: {
    screen: SearchOrder
  },
  SampleDashboard: {
    screen: AppDrawerNavigator
  },
  // DriverList:{
  //   screen: DriverList
  // },
},
  {
    initialRouteName: 'SampleDashboard',
    // initialRouteName:'DriverList',
  });

const AuthStack = createStackNavigator({
  Home: Home,
  ForgotPassword: ForgotPassword
},
  {
    initialRouteName: 'Home',
  });

//   const AppContainer  = createAppContainer(
//   createSwitchNavigator(
//     {
//       App: AppStack,
//       Auth: AuthStack,
//     },
//     {
//       initialRouteName: this.'Auth',
//     }
//   )
// );

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogin: false,
      loaded: false,
    }
  }
  componentDidMount() {
    const iosConfig = {
      clientId: '601376659172-ifmfkj8edupfj0a46p8rp516krgt5aml.apps.googleusercontent.com',
      appId: '1:601376659172:ios:828b812eedc690873efef1',
      apiKey: 'AIzaSyB9S0eb5X1IwDLT4QwvSV4YupNAwjVDVUQ',
      databaseURL: 'https://taxiyefood.firebaseio.com',
      storageBucket: 'taxiyefood.appspot.com',
      messagingSenderId: '601376659172',
      projectId: 'taxiyefood',
      // enable persistence by adding the below flag
      persistence: true,
    }

    const androidConfig = {
      clientId: '601376659172-iaoh2984ujn2e3m3ti4cr38goqf85c2i.apps.googleusercontent.com',
      appId: '1:601376659172:android:a9493736af73495a3efef1',
      apiKey: 'AIzaSyBicog1eiMvajOaiJJk4gulrRR4ccN3GSE',
      databaseURL: 'https://taxiyefood.firebaseio.com',
      storageBucket: 'taxiyefood.appspot.com',
      messagingSenderId: '601376659172',
      projectId: 'taxiyefood',
      // enable persistence by adding the below flag
      persistence: true,
    }
    firebase.initializeApp(
      Platform.OS === 'ios' ? iosConfig : androidConfig,
      'taxiyefood'
    );
    SplashScreen.hide()
    this.checkPermission();
    this.createNotificationListeners();
    this.getData();
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        console.log('fcmToken:', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    console.log('fcmToken:', fcmToken);
  }

  //3
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  showNotification(data) {
    console.log("ShowingNotification:" + JSON.stringify(data))
    const channel = new firebase.notifications.Android.Channel('TaxiyeFoodRestaurant', 'TaxiyeFoodRestaurant', firebase.notifications.Android.Importance.Max)
    firebase.notifications().android.createChannel(channel);
    const notification = new firebase.notifications.Notification()
      .setNotificationId(data.notificationID)
      .setTitle(data.title)
      .setBody(data.body)
      .android.setBigText(data.body)
      .setSound("default")
      .android.setAutoCancel(true)
      .setData(data)
      .android.setChannelId('TaxiyeFoodRestaurant')

    firebase.notifications().displayNotification(notification).then(() => console.log("Show")).catch(err => console.error("error " + err));
  }

  async createNotificationListeners() {

    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((res) => {
      console.log("NOTI_onNotification:", res)
      // console.log(JSON.stringify(notification))
      AsyncStorage.getItem("isLogin", (error, result) => {
        if (!error && result) {
          if (res && res.data) {
            let data = res.data;
            if (data.type) {
              if (data.type == 'customer' || data.type == 'driver') {
                console.log("received initial notification " + JSON.stringify(data));
                DeviceEventEmitter.emit(Constants.EVENTS.orderNotification)
              }
              // DeviceEventEmitter.emit(Constants.EVENTS.orderNotification)
            }
            this.showNotification(data)
          }
        }
      })
    });



    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((res) => {
        console.log("NOTI_onNotificationOpened")
        AsyncStorage.getItem("isLogin", (error, result) => {
          if (!error && result) {
            if (res && res.notification) {
              // console.log("RES_FOUND1:" + this.props.navigation.state.routeName)
              let notification = res.notification;
              console.log("received initial notification " + JSON.stringify(notification.data));
              if (notification.data && notification.data.type && (notification.data.type == 'customer' || notification.data.type == 'driver')) {
                NavigationService.navigate('SampleDashboard')
              }
            } else {
              console.log("RES_NOT_FOUND1")
            }
          }
        })
      });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */

    firebase
      .notifications()
      .getInitialNotification()
      .then(res => {
        console.log("NOTI_getInitialNotification:")
        if (res) {
        }
      });

    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log("NOTI_onMessage:")
      console.log(JSON.stringify(message))
      //process data message
      // console.log('NOTIFICATION4');
      // console.log("JSON.stringify:", JSON.stringify(message));
    });



  }

  getData = async () => {
    const isLogin = await AsyncStorage.getItem('isLogin');
    this.setState({
      isLogin: isLogin,
      loaded: true,
    })
    console.log("IS_LOGIN:" + isLogin);
    // NavigationService.navigate(isLogin ? 'App' : 'Auth');
  };

  render() {
    const AppContainer = createAppContainer(
      createSwitchNavigator(
        {
          AppStack: AppStack,
          AuthStack: AuthStack,
        },
        {
          initialRouteName: this.state.isLogin ? 'AppStack' : 'AuthStack',
        }
      )
    );
    return (
      <>
        {
          this.state.loaded ?
            <AppContainer
              ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
              }}
            /> : <View></View>
        }
      </>
    );
  }
}

export default App;
