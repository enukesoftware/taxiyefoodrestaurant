import React, { Component } from 'react'
import {
    Platform,
    StatusBar,
    View,
    Text,
    Image,
    Dimensions,
    ActivityIndicator,
    Modal,
    DeviceEventEmitter,
    SafeAreaView,
    Alert,
    TouchableOpacity
} from 'react-native';
import styles from './styles'
import DashboardOrderItem from '../ListItem/DashboardOrderItem'
import Constants from '../../constants/constants'
import Images from '../../constants/images'
import NavigateServices from '../../services/NavigationServices'
import moment from "moment";
import * as ApiManager from './../../services/ApiManager'
import { createAppContainer, NavigationEvents } from "react-navigation";
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';
import CommonHeader from '../custom/CommonHeader'
import ViewPagerAdapter from 'react-native-tab-view-viewpager-adapter';
import AsyncStorage from '@react-native-community/async-storage'
import NavigationServices from '../../services/NavigationServices';
import StatusModal from '../custom/StatusModal'
import { callUpdateDeviceTokenApi } from '../../utils/GlobalFunctions';



class NewOrder extends Component {
    constructor() {
        super();
        this.state = {
            newOrderData: [],
            loading: false,
            resId: null,
        }
        this.loaderFunction = this.loaderFunction.bind(this);
        this.updateNewOrderData = this.updateNewOrderData.bind(this);
        this.initializeData = this.initializeData.bind(this);
        this.resIdSet = this.resIdSet.bind(this);
    }
    loaderFunction(loaderValue) {
        console.log(loaderValue)
        this.setState({
            loading: loaderValue,
            resId: null
        })
    }
    updateNewOrderData(selectedData, key) {

        console.log("selectedData:", JSON.stringify(selectedData))
        let selectedList = []
        let StatusChangeData = selectedData
        selectedList.push(selectedData)
        let res = this.state.newOrderData
        let result = res.filter(item => !selectedList.some(itemToRemove => item.id === itemToRemove.id));


        this.setState({ newOrderData: result, loading: false }, () => {
            if (key == 7) {
                StatusChangeData.status = "7"
                DeviceEventEmitter.emit('addDataPendingOrderList', StatusChangeData);
            }
            else if (key == 6) {
                StatusChangeData.status = "6"
                DeviceEventEmitter.emit('addDataCompleteOrderList', StatusChangeData);
            }
        })
        //    console.log(result)
        //     var result = res.filter(item => !data.some(itemToRemove => item.id === itemToRemove.id));

        //      console.log(result.length);
        //      console.log(this.state.newData.length)
        //  var res = [{description: 'ab', VolumeWeight: 0.92},
        //   {description: 'cb', VolumeWeight: 0.90}, 
        //   {description: 'ab', VolumeWeight: 0.89}]

        // var selected = [{description: 'ab', VolumeWeight: 0.92}]


        // var result = res.filter(item => !selected.some(itemToRemove => item.VolumeWeight === itemToRemove.VolumeWeight ));

        // console.log(result);

        //  console.log(this.state.Name)
        //    console.log()
        // //    this.setState({value: "Akash"})
        // //    console.log(this.state.Name)
        // //    console.log(this.state.newData)
        // for(let i =0;i<this.state.newData.length;i++){
        //    if( this.state.newData[i].id == data.id ){
        //        console.log(data)
        //    }
        // }
    }

    componentDidMount() {
        DeviceEventEmitter.addListener('newOrderList', orderList => this.initializeData(orderList));
        DeviceEventEmitter.addListener('resId', resId => this.resIdSet(resId));
        //console.log("userId:",userId)


        // this.setState({ loading: true }, () => {
        //     // console.log("LOADING")
        //     this.callNewDataApi()
        // })

    }
    resIdSet(id) {
        this.setState({ resId: id })
        // console.log("New ResId:", this.state.resId)
    }

    initializeData(orderList) {
        // console.log("New ORDER_LIST:" + JSON.stringify(orderList))
        this.setState({ newOrderData: orderList })
    }

    callNewDataApi() {
        const todayDate = moment(new Date()).format("YYYY-MM-DD");
        let body = {
            restaurant_id: "",
            date: "",
            from: " ",
            to: "",
            order_id: "",
            status: "0",
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        ApiManager.orderList(body)
            .then(res => {
                this.setState({ loading: false })
                if (res.success) {
                    if (res.data != null) {
                        jsonData = JSON.stringify(res.data.data)
                        value = JSON.parse(jsonData)

                        var data_filter = value.filter(element => element.status == 0)

                        this.setState({ newOrderData: data_filter })
                        this.setState({ loading: false })
                        console.log(data_filter)
                    }
                }

            }).catch(err => {
                this.setState({ loading: false })
                console.log(err)
            })
    }
    render() {
        const { newOrderData } = this.state;
        return (
            <View style={{ flex: 1, }}>
                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.loading}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <ActivityIndicator
                                animating={this.state.loading} />
                        </View>
                    </View>
                </Modal>
                <ScrollView >
                    <View style={{ flex: 1, }}>
                        {newOrderData
                            ? newOrderData.map((item, key) => (
                                <DashboardOrderItem
                                    key={key}
                                    newValue={item}
                                    type='1'
                                    updateNewOrderData={this.updateNewOrderData}
                                    loaderFunction={this.loaderFunction}
                                    resId={this.state.resId}

                                ></DashboardOrderItem>
                            )) : null
                        }
                    </View>
                </ScrollView>

            </View>
        );
    }
}

// NewOrder.navigationOptions = {
// tabBarIcon: ({ tintColor, focused }) => (
//     <Image source={Images.search}
//         style={{ marginRight: 15, height: 20, width: 20 }}
//     ></Image>
// ),
// tabBarLabel:({tintColor,focused})=>(
//     <Text style={{color:'green'}}>New Order</Text>
// )
// width:'10%',
// }

class PendingOrder extends Component {
    constructor() {
        super();
        this.state = {
            pendingData: [],
            loading: false,
            resId: null,
            currentOrderStatus: '7',
            statusModalVisible: false,
            selectedOrderData: null,
        }
        this.initializeData = this.initializeData.bind(this);
        this.resIdSet = this.resIdSet.bind(this);
        this.loaderFunction = this.loaderFunction.bind(this);
    }

    loaderFunction(loaderValue) {
        console.log(loaderValue)
        this.setState({
            loading: loaderValue,
            resId: null
        })
    }
    componentDidMount() {
        // this.setState({ loading: true }, () => {
        //     //console.log("LOADING")
        //     this.callPendingDataApi()
        // })
        DeviceEventEmitter.addListener('addDataPendingOrderList', selectedData => this.addData(selectedData));
        DeviceEventEmitter.addListener('pendingOrderList', orderList => this.initializeData(orderList));
        DeviceEventEmitter.addListener('resId', resId => this.resIdSet(resId));
        // console.log("userId:",userId)
        //console.log(DeviceEventEmitter.addListener('eventKey',data))
    }
    resIdSet(id) {
        this.setState({ resId: id })
        //console.log("UserId:",this.state.loginUserId)

    }
    initializeData(orderList) {
        console.log("Pending ORDER_LIST:" + JSON.stringify(orderList))
        this.setState({ pendingData: orderList })
    }

    addData(selectedData) {

        let data = this.state.pendingData
        if (!data || data.length == 0) {
            // this.callPendingDataApi()
            data = []
            data.push(selectedData)
            this.setState({ pendingData: data })
        }
        else {
            data.push(selectedData)
            this.setState({ pendingData: data })
            console.log(data)
        }
    }

    handleStatusModalVisibility = (visibility) => {
        if (visibility != this.state.statusModalVisible) {
            this.setState({ statusModalVisible: visibility })
        }
    }

    onPendingOrderStatusChanged = (data) => {
        // console.log("SELECTED:" + JSON.stringify(data))
        this.setState({
            currentOrderStatus: (data.status == '0' || data.status == '1' || data.status == '7') ? '7' : (data.status == '2' || data.status == '3') ? '2' : data.status,

        }, () => {
            this.statusModel.setDefaultStatus(this.state.currentOrderStatus)
            this.setState({
                selectedOrderData: data,
                statusModalVisible: true
            })
            // this.handleStatusModalVisibility(true)
        })
    }

    handleStatusUpdate(callBack) {
        this.setState({ statusModalVisible: false }, () => {
            this.setState({
                loading: true
            })
            setTimeout(() => {
                this.callStatusUpdateApi(callBack)
            }, 100);
        });
    }

    callStatusUpdateApi(callBack) {
        // let data = {
        //     ...this.state.selectedOrderData,
        //     status: callBack.key
        // }

        // this.setState({ statusModalVisible: false, loading: true });
        let oldData = this.state.selectedOrderData
        let newData = this.state.selectedOrderData
        let index = this.state.pendingData.findIndex(item => item.id === oldData.id);

        newData.status = callBack.key

        // console.log("STATUS:" + callBack.key)
        // console.log("Data:", JSON.stringify(data))
        let body = {
            order_id: newData.id,
            status: newData.status,
        }
        // let loaderValue = true
        // this.props.loaderFunction(loaderValue)
        console.log("Pending Data:", body)


        ApiManager.updateOrderList(body)
            .then(res => {
                if (res.success) {
                    // let loaderValue = false
                    // this.props.loaderFunction(loaderValue)
                    console.log("UpdateApi:", JSON.stringify(res))
                    if (callBack.key == Constants.ORDERS_STATUS.Delivered || callBack.key == Constants.ORDERS_STATUS.Cancelled) {
                        let selectedList = []
                        // let StatusChangeData = newData
                        selectedList.push(newData)
                        let res = this.state.pendingData
                        let result = res.filter(item => !selectedList.some(itemToRemove => item.id === itemToRemove.id));


                        this.setState({ pendingData: result,loading:false }, () => {
                            DeviceEventEmitter.emit('addDataCompleteOrderList', newData);
                        })
                    } else {
                        const newArray = [...this.state.pendingData];
                        newArray[index] = newData;
                        JSON.stringify("UPDATED_DATA1:" + JSON.stringify(newArray))
                        this.setState({ pendingData: newArray, loading: false });
                    }

                } else {
                    const newArray = [...this.state.pendingData];
                    newArray[index] = oldData;
                    JSON.stringify("UPDATED_DATA2:" + JSON.stringify(newArray))
                    this.setState({ pendingData: newArray, loading: false });
                }
            }).catch(err => {
                console.log(err)
                const newArray = [...this.state.pendingData];
                newArray[index] = oldData;
                JSON.stringify("UPDATED_DATA3:" + JSON.stringify(newArray))
                this.setState({ pendingData: newArray, loading: false });
            })

        // const newArray = [...this.state.pendingData];
        // newArray[index] = newData;
        // JSON.stringify("UPDATED_DATA1:" + JSON.stringify(newArray))
        // this.setState({ pendingData: newArray, loading: false });
    }

    callPendingDataApi() {
        var todayDate = moment(new Date()).format("YYYY-MM-DD");
        let body = {
            restaurant_id: "",
            date: "",
            from: " ",
            to: todayDate,
            order_id: "",
            status: "",
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        ApiManager.orderList(body)
            .then(res => {
                // this.setState({loading:false})
                if (res.success) {
                    if (res.data != null) {
                        jsonData = JSON.stringify(res.data.data)
                        newValue = JSON.parse(jsonData)
                        // this.setState({pendingData:newValue})
                        var data_filter = newValue.filter(element => element.status == 7 || element.status == 2 || element.status == 3 || element.status == 4)
                        this.setState({ pendingData: data_filter })
                        this.setState({ loading: false })
                        console.log(data_filter)
                    }
                }
            }).catch(err => {
                // this.setState({loading:false})
                console.log(err)
            })
    }

    render() {
        const { pendingData } = this.state;
        return (
            <View style={{ flex: 1, }}>
                <StatusModal
                    ref={statusModel => this.statusModel = statusModel}
                    currentStatus={this.state.currentOrderStatus}
                    statusModalVisible={this.state.statusModalVisible}
                    closeModal={() => this.handleStatusModalVisibility(false)}
                    okPressed={(callBack) => this.handleStatusUpdate(callBack)}
                />
                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.loading}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <ActivityIndicator
                                animating={this.state.loading} />
                        </View>
                    </View>
                </Modal>
                <ScrollView>
                    <View style={{ flex: 1, }}>
                        {pendingData ? pendingData.map((item, key) => (
                            <DashboardOrderItem
                                key={key}
                                pendingValue={item}
                                type='2'
                                resId={this.state.resId}
                                loaderFunction={this.loaderFunction}
                                onPendingOrderStatusChanged={this.onPendingOrderStatusChanged}
                            ></DashboardOrderItem>
                        )) : null
                        }

                    </View>
                </ScrollView>
            </View>
        );
    }
}

class CompletedOrder extends Component {
    state = {
        completeData: [],
        loading: false,
        resId: null
    }
    constructor() {
        super();
        this.initializeData = this.initializeData.bind(this);
        this.resIdSet = this.resIdSet.bind(this);
    }
    componentDidMount() {
        // this.setState({ loading: true }, () => {
        //     //console.log("LOADING")
        //     this.callNewDataApi()
        // })
        DeviceEventEmitter.addListener('addDataCompleteOrderList', selectedData => this.addData(selectedData));
        DeviceEventEmitter.addListener('completeOrderList', orderList => this.initializeData(orderList));
        DeviceEventEmitter.addListener('resId', resId => this.resIdSet(resId));

    }
    resIdSet(id) {
        this.setState({ resId: id })
        //console.log("UserId:",this.state.loginUserId)

    }

    initializeData(orderList) {
        this.setState({ completeData: orderList })
    }

    addData(selectedData) {
        let data = this.state.completeData
        if (!data || data.length == 0) {
            // this.callNewDataApi()
            data = []
            data.push(selectedData)
            this.setState({ completeData: data })
        }
        else {
            data.push(selectedData)
            this.setState({ completeData: data })
        }
        //    console.log(data)
    }
    callNewDataApi() {
        var todayDate = moment(new Date()).format("YYYY-MM-DD");
        let body = {
            restaurant_id: "",
            date: "",
            from: " ",
            to: todayDate,
            order_id: "",
            status: "5",
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        ApiManager.orderList(body)
            .then(res => {
                this.setState({ loading: false })
                if (res.success) {
                    if (res.data != null) {
                        jsonData = JSON.stringify(res.data.data)
                        value = JSON.parse(jsonData)
                        var data_filter = value.filter(element => element.status == 5 || element.status == 6 || element.status == 7)
                        this.setState({ completeData: data_filter })
                        this.setState({ loading: false })
                        console.log(data_filter)
                    }
                }
            }).catch(err => {
                this.setState({ loading: false })
                console.log(err)
            })
    }
    render() {
        const { completeData } = this.state;
        //  if(!completeData){return null}
        return (
            <View style={{ flex: 1, }}>
                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.loading}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <ActivityIndicator
                                animating={this.state.loading} />
                        </View>
                    </View>
                </Modal>
                <ScrollView>

                    <View style={{ flex: 1, }}>
                        {completeData ? completeData.map((item, key) => (
                            <DashboardOrderItem
                                key={key}
                                completeValue={item}
                                type='3'
                                resId={this.state.resId}
                            ></DashboardOrderItem>
                        )) : null
                        }
                        {/* <DashboardOrderItem
                        type='3'
                        jsonValue = {this.state.completeData}
                        ></DashboardOrderItem> */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// const TabNavigator = createMaterialTopTabNavigator({
//     NewOrder,
//     PendingOrder,
//     CompletedOrder
// });

// const SampleDashboard = createAppContainer(TabNavigator);

// export default SampleDashboard;






const DashboardTabNavigator = createMaterialTopTabNavigator(
    {
        'NEW ORDER': {
            screen: NewOrder,
        },
        'CONFIRMED ORDER': {
            screen: PendingOrder,
        },
        'COMPLETED ORDER': {
            screen: CompletedOrder,
        }
    },
    {
        initialRouteName: 'NEW ORDER',
        pagerComponent: ViewPagerAdapter,
        swipeEnabled: true,
        tabBarOptions: {
            swipeEnabled: true,
            tabStyle: {
                width: 145,
                paddingHorizontal: 0,
                paddingVertical: 2,
                // minWidth:'50%',
                // flexWrap:'wrap-reverse',
            },

            scrollEnabled: true,
            activeTintColor: 'white',
            inactiveTintColor: 'white',
            showIcon: false,
            style: {
                backgroundColor: Constants.color.primary,
                // width:'auto',
            },
            labelStyle: {
                fontSize: Constants.fontSize.SmallXX,
                // fontWeight: 'bold',
                fontFamily: Constants.fontFamily.Bold,
            },
            indicatorStyle: {
                backgroundColor: 'white',
                // height:10,
                width: 145,
                // flex:0.7,
                // paddingHorizontal:10,
                // width:'150',
                // minWidth:'50%',
                // paddingHorizontal:5,
            }
        },
        // navigationOptions: ({ navigation }) => {
        //     const { routeName } = navigation.state.routes[navigation.state.index]
        //     return {
        //         title: 'Biryani Restaurant',
        //         headerStyle: {
        //             backgroundColor: Constants.color.primary,
        //         },
        //         headerTitleStyle: {
        //             color: 'white',
        //             fontSize: Constants.fontSize.NormalX,
        //             fontFamily: Constants.fontFamily.Bold,
        //             width: Dimensions.get('window').width - 80,
        //         },
        //         headerTintColor: 'white',
        //         headerLeft: null,
        //         headerRight: (
        //             <View style={{ flexDirection: 'row' }}>
        //                 <TouchableOpacity onPress={() => NavigateServices.navigate('SearchOrder')} >
        //                     <Image source={Images.search}
        //                         style={{ marginRight: 15, height: 20, width: 20 }}
        //                     ></Image>
        //                 </TouchableOpacity>
        //                 <TouchableOpacity onPress={() => showLogoutAlert()} >
        //                     <Image source={Images.logout}
        //                         style={{ marginRight: 15, height: 20, width: 20 }}
        //                     ></Image>
        //                 </TouchableOpacity>
        //             </View>
        //         )
        //     };
        // }

    }
);

// export default DashboardTabNavigator

const DashboardContainer = createAppContainer(DashboardTabNavigator);

class Dashboard extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: null,
        swipeEnabled: true,
    });

    constructor() {
        super();
        this.state = {
            loading: false,
            isFirstTime: true,
            unMounted: false,
            userData: null,
        }
        this.callOrderListApi = this.callOrderListApi.bind(this);
        this.showLogoutAlert = this.showLogoutAlert.bind(this);
        this.callLogoutApi = this.callLogoutApi.bind(this);
        this.orderNotificationHandler = this.orderNotificationHandler.bind(this);
    }

    orderNotificationHandler(){
        console.log("EVENT")
        if(this.interval){
            clearInterval(this.interval)
        }
        this.setState({
            loading: true,
            unMounted: false,
        }, () => {
            this.interval = setInterval(this.callOrderListApi, 1 * 60 * 1000);
            this.firstCallOrderListApi()
        })
    }

    showLogoutAlert = () => {
        Alert.alert("Logout", "Are you sure want to logout?",
            [
                {
                    text: "OK",
                    onPress: () => { this.setState({ loading: true, }, () => { this.callLogoutApi() }) }
                },
                {
                    text: "Cancel",
                    onPress: () => { }
                },
            ])
    }

    callLogoutApi = () => {
        ApiManager.userLogout(this.state.userData.user_details.id)
            .then(res => {
                console.log("RES1:")
                this.setState({ loading: false })
                console.log("RES2:" + JSON.stringify(res))
                if (res.success) {
                    AsyncStorage.removeItem("isLogin");
                    AsyncStorage.removeItem('userData');
                    NavigationServices.navigate("Home")
                } else if (res.message) {
                    setTimeout(() => {
                        Alert.alert(res.message);
                    }, 1000)
                }
            }).catch(err => {
                this.setState({ loading: false })
                console.log(err)
            })
    }

    async callOrderListApi() {
        console.log("API_CALL User List:"+this.state.unMounted+ " "+this.state.loading)
        var todayDate = moment(new Date()).format("YYYY-MM-DD");
        let resId = this.state.userData.restaurent_details.restaurent_id
        let body = {
            restaurant_id: resId,
            date: "",
            from:  todayDate,
            to: todayDate,
            order_id: "",
            status: "",
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        console.log("Body:", body)
        ApiManager.orderList(body)
            .then(res => {
                console.log("RES1:")
                if (this.state.unMounted) { return }
                this.setState({ loading: false, isFirstTime: false })
                console.log("RES2:")
                if (res.success) {
                    if (res.data != null) {
                        jsonData = JSON.stringify(res.data.data)
                        value = JSON.parse(jsonData)
                        // console.log("RES:" + jsonData)
                        // console.log("value:", value)
                        let newOrderList = value.filter(element => element.status == 0 || element.status == 1)
                        let pendingOrderList = value.filter(element => element.status == 7 || element.status == 2 || element.status == 3 || element.status == 4)
                        let completedOrderList = value.filter(element => element.status == 5 || element.status == 6)

                        DeviceEventEmitter.emit('pendingOrderList', pendingOrderList);
                        DeviceEventEmitter.emit('completeOrderList', completedOrderList);
                        DeviceEventEmitter.emit('newOrderList', newOrderList);
                        //console.log("UserID:",UserId)
                        DeviceEventEmitter.emit('resId', resId);
                    }
                }
            }).catch(err => {
                this.setState({ loading: false })
                console.log(err)
            })
    }
    firstCallOrderListApi() {
        console.log("First Time API_CALL User List:")
        var todayDate = moment(new Date()).format("YYYY-MM-DD");
        let resId = this.state.userData.restaurent_details.restaurent_id
        let body = {
            restaurant_id: resId,
            date: "",
            from: todayDate,
            to: todayDate,
            order_id: "",
            status: "",
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        console.log("Body:", JSON.stringify(body))
        ApiManager.orderList(body)
            .then(res => {
                if (this.state.unMounted) { return }
                this.setState({ loading: false, isFirstTime: false })
                if (res.success) {
                    if (res.data != null) {
                        jsonData = JSON.stringify(res.data.data)
                        value = JSON.parse(jsonData)
                        // console.log("RES:" + jsonData)
                        // console.log("value:", value)
                        // let newOrderList = value.filter(element => element.status == 0)
                        // let pendingOrderList = value.filter(element => element.status == 1 || element.status == 2 || element.status == 3 || element.status == 4)
                        // let completedOrderList = value.filter(element => element.status == 5 || element.status == 6 || element.status == 7)

                        let newOrderList = value.filter(element => element.status == 0 || element.status == 1)
                        let pendingOrderList = value.filter(element => element.status == 7 || element.status == 2 || element.status == 3 || element.status == 4)
                        let completedOrderList = value.filter(element => element.status == 5 || element.status == 6)
                        DeviceEventEmitter.emit('pendingOrderList', pendingOrderList);
                        DeviceEventEmitter.emit('completeOrderList', completedOrderList);
                        DeviceEventEmitter.emit('newOrderList', newOrderList);
                        DeviceEventEmitter.emit('resId', resId);

                    }
                }
            }).catch(err => {
                this.setState({ loading: false })
                console.log(err)
            })
    }

    componentDidUpdate() {
        console.log("componentDidUpdate");
    }

    render() {
        const { isFirstTime } = this.state
        return (
            <View style={{ flex: 1 }}>

                <NavigationEvents
                    onDidFocus={() => {
                        console.log('did focus')
                        DeviceEventEmitter.addListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
                        AsyncStorage.getItem('userData').then((res) => {
                            if (res) {
                                this.setState({
                                    userData: JSON.parse(res)
                                }, () => {
                                    if(this.interval){
                                        clearInterval(this.interval)
                                    }
                                    if (isFirstTime) {
                                        this.setState({
                                            loading: true,
                                            unMounted: false,
                                        }, () => {
                                            this.interval = setInterval(this.callOrderListApi, 1 * 60 * 1000);
                                            this.firstCallOrderListApi()
                                            callUpdateDeviceTokenApi();
                                        })
                                    } else {
                                        // this.interval = setInterval(this.callOrderListApi, 30000);
                                        // this.callOrderListApi()
                                        this.setState({
                                            unMounted: false,
                                        }, () => {
                                            this.interval = setInterval(this.callOrderListApi, 1 * 60 * 1000);
                                            // this.callOrderListApi()
                                        })
                                    }
                                })
                            }
                        }).catch((err) => {
                            console.log("ERROR:" + JSON.stringify(err))
                        })



                    }}
                    onWillBlur={() => {
                        DeviceEventEmitter.removeListener(Constants.EVENTS.orderNotification, this.orderNotificationHandler)
                        this.setState({
                            unMounted: true,
                        }, () => {
                            clearInterval(this.interval)
                        })
                        // clearInterval(this.interval)
                        console.log('will blur')
                    }}
                />
                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.loading}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <ActivityIndicator
                                animating={this.state.loading} />
                        </View>
                    </View>
                </Modal>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <StatusBar barStyle="light-content" backgroundColor={Constants.color.primaryDark} />
                <CommonHeader title={this.state.userData && this.state.userData.restaurent_details && this.state.userData.restaurent_details.name ? this.state.userData.restaurent_details.name : "Restaurant"} noBack={true} twoMenu={true}
                    firstMenuEvent={() => {
                        NavigateServices.navigate("SearchOrder")
                    }}
                    secondMenuEvent={() => {
                        this.showLogoutAlert()
                    }}
                    firstMenuImg={Images.search}
                    secondMenuImg={Images.logout} />
                <DashboardContainer />
            </View>
        )
    }
}

export default Dashboard
