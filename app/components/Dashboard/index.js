import React, { Component } from 'react'
import { Platform, StyleSheet, View, Text, ScrollView } from 'react-native';
import styles from './styles'
import DashboardOrderItem from '../ListItem/DashboardOrderItem'
import Constants from '../../constants/constants'
import {  createMaterialTopTabNavigator } from 'react-navigation-tabs';
// import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";

import AsyncStorage from '@react-native-community/async-storage';

class Dashboard extends Component {

    

    static navigationOptions = {
        title: 'Restaurent Name',
        headerStyle: {
            backgroundColor: Constants.color.primary,
        },
        headerTitleStyle: {
            color: 'white',
            fontWeight: 'bold'
        },
    };

    //  constructor(props) {
    //         super(props)
    state = {
        userData: ''
    }
    //  }
    
    componentWillMount() {
        AsyncStorage.getItem('userData', (error, result) => {
            if (error) {

            }
            else {
                this.setState({ userData: JSON.parse(result) })
                console.warn(JSON.parse(result));
                console.log(JSON.parse(result));
            }
        })
    }
    render() {
        console.log(this.state.userData)
        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.pageBackgroundColor }}>
                {/* <Text>{this.state.userData.first_name}</Text> */}
                <ScrollView>
                    <DashboardOrderItem />

                </ScrollView>
            </View>
        );
    };
}

export default Dashboard