import { StyleSheet } from 'react-native'
import Constants from '../../constants/constants'

export default StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: Constants.blue
    },
    activityIndicatorWrapper:{
        backgroundColor: Constants.color.progressDialogColor,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        opacity: 0.5,
    },
})