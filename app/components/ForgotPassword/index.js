import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from "react-navigation";

import {
    Platform,

    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    Icon,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// import styles from './styles'
import TextInput from 'react-native-material-textinput'
import Constants from './../../constants/constants'
import AlertManager from './../../constants/alert'
import styles from './styles'
import * as ApiManager from './../../services/ApiManager'
import Images from '../../constants/images'
import { Card } from '../custom/Card';
import CustomText from '../custom/CustomText'
import NavigationServices from '../../services/NavigationServices';


// import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
// import icoMoonConfig from '../../../selection.json';
// const Linericon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class ForgotPassword extends Component {

    static navigationOptions = ({ navigation }) => ({
        header: null
    });

    constructor() {
        super()
        this.state = {
            username: 'velle@gmail.com',
            loading: false,
        }
    }

    onTextChange = key => value => {
        this.setState({
            [key]: value
        })
    }

    buttonPressed(){}

    buttonPressed() {
        const { username } = this.state;

        if (!this.isValidate()) return

        this.setState({
            loading: true,
        })

        ApiManager.forgotPassword(username)
            .then(res => {
                this.setState({
                    loading: false,
                },()=>{
                    if (res.success) {
                        setTimeout(() => {
                            AlertManager.showAlert(res.message)
                        }, 500)
                        NavigationServices.navigate('Home')
                    } else {
                        setTimeout(() => {
                            AlertManager.showAlert(res.message)
                        }, 500)
                    }
                })
               
            }).catch(err => {
                this.setState({
                    loading: false,
                })

                setTimeout(() => {
                    AlertManager.showAlert('Error', err, 'ok')
                }, 500)
            })

    }

    isValidate() {
        // const {username, password} = this.state;
        if (this.state.username == '') {
            AlertManager.showAlert('Alert', 'Please enter username', 'Ok')
            return false;
        }

        let emailValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!emailValidator.test(this.state.username)) {
            AlertManager.showAlert('Alert', 'Email is not correct', 'Ok')
            return false;
        }

        return true;

    }



    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Modal
                        transparent={true}
                        animationType={'none'}
                        visible={this.state.loading}
                        onRequestClose={() => { console.log('close modal') }}>
                        <View style={styles.modalBackground}>
                            <View style={styles.activityIndicatorWrapper}>
                                <ActivityIndicator
                                    animating={this.state.loading} />
                            </View>
                        </View>
                    </Modal>

                    <Image style={styles.LogoImageStyle} source={Images.logoText}></Image>

                    <Text style={styles.headerText}>Forgot Password</Text>

                    <CustomText container={styles.CustomTextStyle}
                        placeholder='Email Id'
                        icon={Images.userName}
                        value = {this.state.username}
                        onTextChange={this.onTextChange('username')}
                    />


                    <TouchableOpacity onPress={() => this.buttonPressed()} style={styles.touchable}>
                        <Text style={styles.btnText}>Send</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={styles.ForgotTouchableStyle} onPress={()=>{NavigationServices.navigate('Home')}}>
                        <Text style={styles.ForgotText}>Go to</Text>
                        <Text style={styles.ForgotResetText}> Login</Text>
                    </TouchableOpacity>




                </View>
            </SafeAreaView>
        );
    };
}


export default ForgotPassword