import { StyleSheet } from 'react-native';
import Constants from './../../constants/constants'


export default StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20,
        justifyContent:'center'
    },
    LogoImageStyle:{
        alignSelf:'center',
        width:90,
        height:90,
        marginBottom:50
    },
    headerText: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom:10
    },
   
    touchable: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        padding: 10,
        marginTop: 20,
        borderRadius:5,
        alignSelf:'center',
        width:Constants.login.commonWidth
    },
    btnText: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        fontWeight: 'bold',
    },
    activityIndicatorWrapper:{
        backgroundColor: Constants.color.progressDialogColor,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        opacity: 0.5,
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: Constants.primary
    },
    CustomTextStyle:{
        marginTop:15,
        width:Constants.login.commonWidth,
        alignSelf:'center'
    },
    ForgotTouchableStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 10,
        marginTop: 0,
        borderRadius:5,
        alignSelf:'center',
        width:Constants.login.commonWidth
    },
    ForgotText: {
        fontSize:12,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'grey',

    },
    ForgotResetText: {
        fontSize:12,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
    },
});