import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from "react-navigation";

import {
    Platform,

    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    Icon,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// import styles from './styles'
import TextInput from 'react-native-material-textinput'
import Constants from './../../constants/constants'
import AlertManager from './../../constants/alert'
import styles from './styles'
import * as ApiManager from './../../services/ApiManager'
import Images from '../../constants/images'
import { Card } from '../custom/Card';
import CustomText from '../custom/CustomText'
import NavigationServices from '../../services/NavigationServices';
import { callUpdateDeviceTokenApi } from '../../utils/GlobalFunctions';


// import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
// import icoMoonConfig from '../../../selection.json';
// const Linericon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Home extends Component {

    static navigationOptions = ({ navigation }) => ({
        header: null
    });

    constructor() {
        super()
        this.state = {
            username2: 'velle@gmail.com',
            password2: 'vijayanand',
            username:'kajal.bansal8@enukesoftware.com',
            password:'123456',
            loading: false,
        }
    }

    onTextChange = key => value => {
        this.setState({
            [key]: value
        })
    }

    buttonPressed2(){
        this.props.navigation.navigate('SampleDashboard')
    }

    buttonPressed() {
        const { username, password } = this.state;

        if (!this.isValidate()) return

        this.setState({
            loading: true,
        })

        ApiManager.userSignIn(username, password)
            .then(res => {
                this.setState({
                    loading: false,
                })

                if (res.success) {
                    if (res.data != null) {
                        AsyncStorage.setItem("isLogin", "true");
                       console.log("USER_DATA:"+JSON.stringify(res.data)) 
                       // let obj = JSON.parse(res.data);
                      AsyncStorage.setItem('userData', JSON.stringify(res.data));
                      callUpdateDeviceTokenApi();
                        this.props.navigation.navigate('SampleDashboard')
                    }
                } else {
                    setTimeout(() => {
                        AlertManager.showAlert(res.message)
                    }, 500)
                }




                // console.warn(JSON.stringify(res))
            }).catch(err => {
                this.setState({
                    loading: false,
                })

                setTimeout(() => {
                    AlertManager.showAlert('Error', err, 'ok')
                }, 500)
            })

    }

    isValidate() {
        // const {username, password} = this.state;
        if (this.state.username == '') {
            AlertManager.showAlert('Alert', 'Please enter username', 'Ok')
            return false;
        }

        let emailValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!emailValidator.test(this.state.username)) {
            AlertManager.showAlert('Alert', 'Email is not correct', 'Ok')
            return false;
        }

        if (this.state.password == '') {
            AlertManager.showAlert('Alert', 'Please enter password', 'Ok')
            return false;
        }

        return true;

    }



    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Modal
                        transparent={true}
                        animationType={'none'}
                        visible={this.state.loading}
                        onRequestClose={() => { console.log('close modal') }}>
                        <View style={styles.modalBackground}>
                            <View style={styles.activityIndicatorWrapper}>
                                <ActivityIndicator
                                    animating={this.state.loading} />
                            </View>
                        </View>
                    </Modal>

                    {/* <Linericon
                        name='ic_cross_small'
                        size={28}
                        color='green'
                    /> */}

                    <Image style={styles.LogoImageStyle} source={Images.logoText}></Image>

                    <Text style={styles.headerText}>Log In</Text>

                    <CustomText container={styles.CustomTextStyle}
                        placeholder='Username'
                        icon={Images.userName}
                        value = {this.state.username}
                        onTextChange={this.onTextChange('username')}
                    />
                    <CustomText container={styles.CustomTextStyle}
                        secureText={true}
                        placeholder='Password'
                        icon={Images.password}
                        value = {this.state.password}
                        onTextChange={this.onTextChange('password')}
                    />


                    {/* <TextInput
                        style={styles.textInput}
                        label='Username'
                        underlineActiveColor={Constants.color.primary}
                        labelActiveColor={Constants.color.primary}
                        onChangeText={text => this.setState({ username: text })} />
                    <TextInput style={styles.textInput}
                        underlineActiveColor={Constants.color.primary}
                        labelActiveColor={Constants.color.primary}
                        secureTextEntry={true}
                        label='Password'
                        onChangeText={text => this.setState({ password: text })} /> */}

                    <TouchableOpacity onPress={() => this.buttonPressed()} style={styles.touchable}>
                        <Text style={styles.btnText}>LOGIN</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={styles.ForgotTouchableStyle} onPress={()=>{NavigationServices.navigate('ForgotPassword')}}>
                        <Text style={styles.ForgotText}>Forgot your Password?</Text>
                        <Text style={styles.ForgotResetText}> Reset It</Text>
                    </TouchableOpacity>




                </View>
            </SafeAreaView>
        );
    };
}


export default Home