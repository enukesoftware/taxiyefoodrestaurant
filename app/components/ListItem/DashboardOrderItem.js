import React, { Component } from 'react'
import { Platform, DeviceEventEmitter, View, Text, Image, TouchableOpacity, TouchableHighlight, Modal, ActivityIndicator, Alert } from 'react-native';
import styles from './styles'
import Constants from '../../constants/constants'
import Images from '../../constants/images'
import { Dropdown } from 'react-native-material-dropdown';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import NavigationServices from '../../services/NavigationServices'
import moment from "moment"
import StatusModal from '../custom/StatusModal'
import * as ApiManager from './../../services/ApiManager'
import DriverItem from './DriverItem'

class DashboardOrderItem extends Component {



    constructor() {
        super()
        this.state = {
            updateData: "",
            selectedValue: "",
            statusModalVisible: false,
            flag: true,
            driver: { "id": "603", "role_id": "4", "profile_image": "http://sfstaging.yiipro.com/images/uploads/user/44619.Anonymous-Whatsapp-profile-picture.jpg", "first_name": "driver ", "last_name": "one", "email": "driverone@gmail.com", "contact_number": "77676675756", "verified": "1", "status": "1", "token": "b3b76aebd871cbd2d7d8f7439dd0a57d", "device_token": "esxKJvjuKRE:APA91bH-JOx7ZoVfYZGtfJauJe4ZkaRvfMPsrFt6-YNnQuK9Y03a4IKO4qbRHp-HoKBh4W09xqTxUrCkImMO-XYB-Mc_FMI3cT0d5pXYTNdjHce1eoo_zTragaBDf0k43D6DnZ37Irke", "created_at": "2019-02-21 16:52:32", "updated_at": "2019-10-11 05:34:52", "deleted_at": "-0001-11-30 00:00:00", "login_type": "", "countrycode": "91", "newsletter": "1", "last_lat": "28.46580734", "last_long": "77.03818344" }
        }
    }

    componentDidMount() {
        //DeviceEventEmitter.addListener("STATUSCHANGED")
    }
    renderHeader(PlacedDateTime, data) {
        // console.log(PlacedDateTime)
        // console.log('TYPE', this.props.type)
        if (this.props.type == '3') {
            return this.renderMultiDateHeader(PlacedDateTime, data);
        } else {
            return this.renderSingleDateHeader(PlacedDateTime, data);
        }
    }

    renderSingleDateHeader(PlacedDateTime, data) {
        return (
            <View style={styles.DashboardItemCardHeaderStyle}>
                <View style={[styles.DashboardItemCardHeaderViewSingleDate, { paddingLeft: 5, width: '50%' }]}>
                    <Text allowFontScaling={false} style={styles.DashboardItemCardHeaderTextNameStyle} >Placed on </Text>
                    <Text allowFontScaling={false} style={styles.DashboardItemCardHeaderTextValueStyle}>{PlacedDateTime}</Text>
                </View>
                <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 5, justifyContent: 'flex-end', }}>
                    <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Regular, }]}>Order ID -:</Text>
                    <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.SemiBold, }]}> {data.order_number}</Text>
                </View>
            </View>
        );
    }

    renderMultiDateHeader(PlacedDateTime, data) {
        return (
            <View style={styles.DashboardItemCardHeaderStyle}>
                <View style={[styles.DashboardItemCardHeaderView, { paddingLeft: 5, width: '50%' }]}>
                    <Text allowFontScaling={false} style={styles.DashboardItemCardHeaderTextNameStyle} >Placed on </Text>
                    <Text allowFontScaling={false} style={styles.DashboardItemCardHeaderTextValueStyle}>{PlacedDateTime}</Text>
                </View>
                <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 5, justifyContent: 'flex-end', }}>
                    <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Regular, }]}>Order ID -:</Text>
                    <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.SemiBold, }]}> {data.order_number}</Text>
                </View>
                {/* <View style={[styles.DashboardItemCardHeaderView, { paddingLeft: 5, alignSelf: 'flex-end' }]}>
                    <Text style={styles.DashboardItemCardHeaderTextNameStyle}>Delivered on </Text>
                    <Text style={styles.DashboardItemCardHeaderTextValueStyle}>Tue 31 Jan, 9:02AM</Text>
                </View> */}
            </View>
        );
    }

    renderItemsText() {
        if (this.props.type == '3') {
            return;
        }
        return (
            <Text style={styles.DashboardItemOrderDetailTextStyle}>ITEMS</Text>
        );
    }

    renderComma(position, total) {
        if (position != total - 1) {
            return (
                <Text style={{ fontSize: Constants.fontSize.SmallX }}> ,</Text>
            );
        }
    }

    renderItems(item) {
        if (this.props.type == '3') {
            return
        }
        //   console.log(item)
        // data = JSON.stringify(value)
        // data = JSON.parse(data)
        this.data = [
            { name: 'Dosa', qty: '200' },
            { name: 'Shake', qty: '10' },
            { name: 'Dosa', qty: '2' },
            { name: 'Shake', qty: '10' },
            { name: 'Dosa', qty: '2' },
            { name: 'Shake', qty: '10' },
            { name: 'Dosa', qty: '2' },
            { name: 'Shake', qty: '10' },
        ]

        var items = []
        for (let i = 0; i < item.length; i++) {
            items.push(
                <View key={i} style={{ flexDirection: 'row', margin: 2, alignItems: 'center' }}>
                    <View>
                        <Text style={{ fontSize: Constants.fontSize.SmallX, fontFamily: Constants.fontFamily.Regular }}>{item[i].product_name} </Text>
                    </View>
                    <View style={{ backgroundColor: Constants.color.cardHeaderColor, borderRadius: 3, paddingLeft: 4, paddingRight: 4, paddingTop: 2, paddingBottom: 2, }}>
                        <Text style={{ fontSize: Constants.fontSize.SmallX, fontFamily: Constants.fontFamily.Medium }}>{item[i].product_quantity}</Text>

                    </View>
                    <View>
                        {this.renderComma(i, item.length)}
                    </View>

                </View>
            )
            // if(item[i].addons_list != null){
            //     for(let j = 0 ; j < (item[i].addons_list).length; j++){
            //         console.log((item[i].addons_list)[j].name)
            //     }

            //     // console.log((item[i].addons_list).length)
            // }
        }

        if (item.length > 0) {
            return (
                <View >
                    <View style={styles.DividerHorizontal} />
                    <View style={{ flexDirection: 'row', padding: 8, width: '99%', flexWrap: 'wrap' }}>
                        {items}
                    </View>
                </View>
            );
        } else {
            return (
                <View>
                    <View style={styles.DividerHorizontal} />
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text>No Items Found</Text>
                    </View>
                </View>
            );
        }


    }

    renderBottomView(data) {
        switch (this.props.type) {
            case '1':
                return this.renderBottomViewNewOrder(data);

            case '2':
                return this.renderBottomViewPendingOrder(data);

            case '3':
                return this.renderBottomViewCompleteOrder(data);

            // default:
            //     console.log('DEFAULT', this.props.type)
            //     break
        }
        // return (
        //     <View>
        //         <View style={styles.DividerHorizontal}></View>

        //         <View style={styles.DashboardItemCardLocationView}>
        //             <Image source={Images.location} style={styles.DashboardItemCardLocationIcon}></Image>
        //             <Text style={styles.DashboardItemCardLocationAddress}>Huda City Center Metro Station, Sector 29</Text>
        //         </View>
        //     </View>
        // );
    }

    declinePressed(data) {
        console.log("declinePressed")
        console.log("data:", data)
        let loaderValue = true
        this.props.loaderFunction(loaderValue)

        // this.props.updateNewOrderData(data, 6)


        let body = {
            order_id: data.id,
            status: "6",
        }
        console.log("Dic Body:", body)
        ApiManager.updateOrderList(body)
            .then(res => {
                if (res.success) {
                    //console.log("declinePressed:",res)
                    this.props.updateNewOrderData(data, 6)
                    // console.log("declinePressed:",res)
                }
            }).catch(err => {
                loaderValue = false
                this.props.loaderFunction(loaderValue)
                console.log(err)
            })
    }
    acceptPressed(data) {
        console.log("acceptPressed")
        let loaderValue = true
        this.props.loaderFunction(loaderValue)

        let body = {
            order_id: data.id,
            status: "7",
        }

        // this.props.loaderFunction(loaderValue = false)
        console.log("body:", body)
        ApiManager.updateOrderList(body)
            .then(res => {
                if (res.success) {
                    console.log("Update Api:", res.success)
                    // loaderValue = false
                    // this.props.loaderFunction(loaderValue)
                    this.props.updateNewOrderData(data, 7)
                    //console.log("acceptPressed:",res)

                }
            }).catch(err => {
                loaderValue = false
                this.props.loaderFunction(loaderValue)
                console.log(err)
            })

        // console.log(data)
    }
    renderBottomViewNewOrder(data) {
        return (
            <View>
                {/* <View style={styles.container}> */}
                <View style={styles.DividerHorizontal}></View>
                <View style={styles.DashboardBottomViewNeworder}>
                    <TouchableOpacity style={styles.DasboardBottomNewOrderTouchableDecline}>
                        <Text onPress={() => this.declinePressed(data)} style={[styles.DasboardBottomNewOrderTextCommon, { color: 'white' }]}>DECLINE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.DasboardBottomNewOrderTouchableAccept}>
                        <Text onPress={() => this.acceptPressed(data)} style={[styles.DasboardBottomNewOrderTextCommon, { color: Constants.color.fieldValueColor }]}>ACCEPT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    handleStatusModalVisibility(e) {
        if (e != this.state.statusModalVisible) {
            this.setState({ statusModalVisible: e })
        }

    }
    handleStatusUpdate(callBack, data) {
        data.status = callBack.key
        this.setState({ flag: !this.state.flag })
        console.log("Data:", data)
        let body = {
            order_id: data.id,
            status: data.status,
        }
        let loaderValue = true
        this.props.loaderFunction(loaderValue)
        console.log("Pending Data:", body)
        ApiManager.updateOrderList(body)
            .then(res => {
                if (res.success) {
                    let loaderValue = false
                    this.props.loaderFunction(loaderValue)
                    console.log("Update Api:", res.success)
                    // loaderValue = false
                    // this.props.loaderFunction(loaderValue)
                    // this.props.updateNewOrderData(data,7)    
                    //console.log("acceptPressed:",res)

                }
            }).catch(err => {
                // loaderValue = false
                // this.props.loaderFunction(loaderValue)
                console.log(err)
            })
    }

    renderStatusChangeModal(data) {
        // console.log("userID:",userID)
        return (
            <StatusModal
                currentStatus={data.status}
                statusModalVisible={this.state.statusModalVisible}
                closeModal={() => this.handleStatusModalVisibility(false)}
                okPressed={(callBack) => this.handleStatusUpdate(callBack, data)}
            // statusChange={()=> this.StatusChange}
            />)
    }

    // onDropdownSelect(value){
    //     console.log(value)
    //     let {flag} = this.state
    //     setTimeout(() => {
    //        Alert.alert("Confirm","Are you Sure to change its Status?",
    //        [
    //           { text:"OK",
    //         onPress:()=>this.setState({flag:!flag})
    //         },
    //         { text:"Cancel",
    //         onPress:()=>this.setState( prevState=>({
    //             flag:!flag
    //         }))
    //         },
    //     ])
    //     }, 1000);
    // }

    renderDriverView(data) {
        let name = "NA"
        if(!data.driver){
            return
        }else{
            console.log("DRIVER:"+JSON.stringify(data))
            if(data.driver.first_name && data.driver.last_name){
                name = data.driver.first_name +" " + data.driver.last_name
            }else if(!data.driver.first_name && !data.driver.last_name){
                name = "NA"
            }else if(!data.driver.first_name && data.driver.last_name){
                name = data.driver.last_name
            }else if(data.driver.first_name && !data.driver.last_name){
                name = data.driver.first_name
            }
        }
        
        
        return (
            <View>
                <View style={styles.DividerHorizontal}></View>
                <View style={styles.DashboardBottomViewCompleteOrder}>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 5, }}>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontSize: Constants.fontSize.SmallXX, fontFamily: Constants.fontFamily.Medium, }]}>Driver </Text>
                    </View>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingLeft: 5, justifyContent: 'flex-end' }}>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontSize: Constants.fontSize.SmallXX, fontFamily: Constants.fontFamily.Medium, }]}> {name}</Text>
                    </View>

                </View>
            </View>
        )
    }

    renderBottomViewPendingOrder(data) {
        let statusValue = this.getStatus(data.status)
        return (
            <View>
                <DriverItem data={data}></DriverItem>
                <View style={styles.DividerHorizontal}></View>
                <View style={styles.DashboardBottomViewPendingOrder}>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 5, }}>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontSize: Constants.fontSize.SmallXX, fontFamily: Constants.fontFamily.Medium, }]}>Order Status</Text>
                        {/* <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { color: Constants.color.fieldValueColor }]}>#46474</Text> */}
                    </View>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingLeft: 5, justifyContent: 'flex-end' }}>
                        {/* <TouchableOpacity onPress={() => this.setState({ statusModalVisible: true })} style={[styles.DropDownTouchableStyle, { justifyContent: 'center' }]}> */}
                        <TouchableOpacity onPress={() => this.props.onPendingOrderStatusChanged(data)} style={[styles.DropDownTouchableStyle, { justifyContent: 'center' }]}>

                            <View style={styles.DropDownInnerViewStyle}>
                                <View style={
                                    { flex: 1, justifyContent: 'center' }
                                }>

                                    <Text style={{
                                        fontSize: Constants.fontSize.SmallXX,
                                        fontFamily: Constants.fontFamily.Medium
                                    }}>{statusValue}</Text>
                                </View>
                                {/* <Dropdown
                                    ref={dropdown => this.dropdown = dropdown }
                                    containerStyle={
                                        { flex: 1,  justifyContent: 'center' }
                                    }
                                    fontSize={Constants.fontSize.SmallXX}
                                    fontFamily={Constants.fontFamily.Medium}
                                    dropdownOffset={{ top: 12, left: 0 }}
                                    placeholderTextColor={Constants.color.defaultTextColor}
                                    baseColor="rgba(255, 255, 255, 1)"
                                    inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                    placeholder='Select Status'                                
                                    data={this.orderStatus}  
                                    value={statusValue}
                                    onChangeText = {(value,index)=>{this.onDropdownSelect(value,index)}}
                                /> */}
                                <Image source={Images.dropdown} style={styles.DropDownArrowStyle} />

                            </View>

                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        );

    }
    getStatus = (status) => {
        //console.log("status:",status)
        let statusValue = ""
        Object.keys(Constants.ORDERS_STATUS).map((key) => {
            const value = Constants.ORDERS_STATUS[key];
            if (status == value) {
                // console.log(key)
                statusValue = key
            }
        })

        return statusValue;
    }

    renderBottomViewCompleteOrder(data) {
        //     let orderID=""
        //    // console.log("DATA:"+JSON.stringify(data.items))
        //     if ( data.items.length > 0){
        //        orderID = data.items[0].order_id
        //        //console.log(orderID)
        //     }
        //     else{
        //         orderID = "NA"
        //         console.log(data)
        //     }
        //console.log(data.items)
        //if (data.items[0].order_id
        return (
            <View>
             <DriverItem data={data}></DriverItem>
                <View style={styles.DividerHorizontal}></View>
                {/* <View style={styles.DashboardItemCardLocationView}> */}
                <View style={styles.DashboardBottomViewCompleteOrder}>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 5, }}>
                        {/* <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Regular, }]}>Order ID -:</Text>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Medium, }]}> {data.order_number}</Text> */}
                    </View>
                    <View style={{ width: '50%', flexDirection: 'row', flexWrap: 'wrap', paddingLeft: 5, justifyContent: 'flex-end' }}>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Regular, }]}>Order Status -:</Text>
                        <Text style={[styles.DashboardBottomCompleteOrderTextCommon, { fontFamily: Constants.fontFamily.Medium, }]}> {this.getStatus(data.status)}</Text>
                    </View>

                </View>
               
            </View>
        );
    }

    onItemClick(data) {
        // this.props.navigation.navigate('OrderDetail')
        if (this.props.type != '1') {
            NavigationServices.navigate('OrderDetail', { jsonData: data })
        }
    }

    render() {
        let PlacedDateTime, CustomerName, DriveName, CustomerAddress, Amount, producedItem, Image_Http_URL, data
        if (this.props.type == '1') {
            // console.log("type:",this.props)
            const { newValue } = this.props
            data = newValue
            customerId = newValue.id
            // let dateTime = new Date(newValue.date + " " + newValue.time);
            PlacedDateTime = moment(newValue.date + " " + newValue.time).format("ddd DD MMM, h:mm A");
            // let customerDetail = JSON.stringify(newValue.customer)
            // let customerData = JSON.parse(customerDetail)
            const customerData = newValue.customer
            if (customerData && customerData.first_name != null) {
                CustomerName = customerData.first_name + " " + customerData.last_name
            }
            else {
                CustomerName = "NA"
            }
            //  CustomerName = customerData.first_name + " " +  customerData.last_name
            address = newValue.ship_json ? JSON.parse(newValue.ship_json) : null
            if (address != null) {
                let house_address = JSON.parse(address.address_json)
                if (house_address != null) {
                    //console.log(add)
                    CustomerAddress = house_address.house_no + " " + house_address.street + " " + house_address.area_name + " " + house_address.city + " " + house_address.state_id

                }
                else {
                    CustomerAddress = "Not Available";
                    //console.log("----------------------------------------")
                }
            }
            else {
                CustomerAddress = "Not Available";
                //console.log("----------------------------------------")
            }

            Amount = newValue.amount
            //  { <----------- Profile Image ------> }
            if (customerData && customerData.profile_image) {
                Image_Http_URL = { uri: customerData.profile_image };
                //console.log(customerData.profile_image)     
            }
            else {
                Image_Http_URL = Images.userDefaultImage
                // console.log("-----------------")
            }
        }
        else if (this.props.type == '2') {
            const { pendingValue } = this.props
            data = pendingValue
            customerId = pendingValue.id
            // let dateTime = new Date(pendingValue.date + " " + pendingValue.time);
            PlacedDateTime = moment(pendingValue.date + " " + pendingValue.time).format("ddd DD MMM, h:mm A");
            // let customerDetail = JSON.stringify(pendingValue.customer)
            // let customerData = JSON.parse(customerDetail)
            const customerData = pendingValue.customer ? pendingValue.customer : null
            if (customerData && customerData.first_name != null) {
                CustomerName = customerData.first_name + " " + customerData.last_name
            }
            else {
                CustomerName = "NA"
            }
            address = pendingValue.ship_json ? JSON.parse(pendingValue.ship_json) : null
            if (address != null) {
                let house_address = JSON.parse(address.address_json)
                if (house_address != null) {
                    //console.log(add)
                    CustomerAddress = house_address.house_no + " " + house_address.street + " " + house_address.area_name + " " + house_address.city + " " + house_address.state_id

                }
                else {
                    CustomerAddress = "Not Available";
                    //console.log("----------------------------------------")
                }
            }
            else {
                CustomerAddress = "Not Available";
            }
            Amount = pendingValue.amount
            //  { <----------- Profile Image ------> }
            if (customerData && customerData.profile_image) {
                Image_Http_URL = { uri: customerData.profile_image };
            }
            else {
                Image_Http_URL = Images.userDefaultImage
                //console.log("-----------------")
            }
        }
        //  Complete Order Data
        else if (this.props.type == '3') {
            //console.log("Com:")
            const { completeValue } = this.props
            data = completeValue
            customerId = completeValue.id
            // let dateTime = new Date(completeValue.date + " " + completeValue.time);
            PlacedDateTime = moment(completeValue.date + " " + completeValue.time).format("ddd DD MMM, h:mm A");
            // let customerDetail = JSON.stringify(completeValue.customer)
            // let customerData = JSON.parse(customerDetail)
            const customerData = completeValue.customer ? completeValue.customer : null
            if (customerData && customerData.first_name) {
                CustomerName = customerData.first_name + " " + customerData.last_name
            }
            else {
                CustomerName = "NA"
            }

            address = completeValue.ship_json ? JSON.parse(completeValue.ship_json) : null
            if (address != null) {
                let house_address = JSON.parse(address.address_json)
                if (house_address != null) {
                    //console.log(add)
                    CustomerAddress = house_address.house_no + " " + house_address.street + " " + house_address.area_name + " " + house_address.city + " " + house_address.state_id

                }
                else {
                    CustomerAddress = "Not Available";
                    //console.log("----------------------------------------")
                }
            }
            else {
                CustomerAddress = "Not Available";
                //console.log("----------------------------------------")
            }
            Amount = completeValue.amount
            //  { <----------- Profile Image ------> }
            if (customerData && customerData.profile_image) {
                Image_Http_URL = { uri: customerData.profile_image };
                //console.log(customerData.profile_image)     
            }
            else {
                Image_Http_URL = Images.userDefaultImage
            }
        }
        if (!PlacedDateTime || !CustomerName || !CustomerAddress || !Amount) {
            return null
        }
        return (
            <TouchableOpacity activeOpacity={1} onPress={() => this.onItemClick(data)} style={styles.DashboardItemCardContainerStyle}>
                {/* Upper Card */}
                <View style={styles.DashboardItemDashboardItemCardContainerStyle}>


                    {this.renderHeader(PlacedDateTime, data)}

                    {/* Profile Detail */}
                    <View style={styles.DashboardItemCardProfileStyle}>
                        <View style={styles.DashboardItemCardProfileInnerStyle}>
                            <Image source={Image_Http_URL} style={styles.DashboardItemCardProfileImageStyle}></Image>
                            <View style={styles.DashboardItemCardProfileDetailView}>
                                <Text style={styles.DashboardItemCardProfileName}>{CustomerName}</Text>
                                <View style={styles.DashboardItemCardProfileContactView}>
                                    <Image source={Images.location} style={styles.DashboardItemCardProfileContactIcon}></Image>
                                    <Text style={styles.DashboardItemCardProfileContactDetail}>{CustomerAddress}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.DashboardItemCardProfileStatusViewStyle}>
                            <View style={styles.DashboardItemCardProfileStatusStyle}>
                                <Text style={styles.DashboardItemCardProfileStatusTextStyle}>$ {Amount}</Text>
                            </View>
                        </View>
                    </View>


                    {this.renderItemsText()}

                    {/* <View style={styles.DividerHorizontal}></View> */}
                    {/* {data.items.map((item,key)=>(
                        key= {key},
                        value= {item},
                        this.renderItems(value)
                     ))
                     } */}

                    {this.renderItems(data.items)}

                    {this.renderBottomView(data)}

                    {this.props.type == 2 ? this.renderStatusChangeModal(data) : null}
                    {/* {console.log(data)} */}

                    {/* {this.renderBottomViewNewOrder()}

                    {this.renderBottomViewPendingOrder()}
                    {this.renderBottomViewCompleteOrder()} */}


                </View>
            </TouchableOpacity>
        );
    };

}

export default DashboardOrderItem