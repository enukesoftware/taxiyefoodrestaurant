import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, Alert } from 'react-native';
import styles from './styles'
import Constants from '../../constants/constants'
import Images from '../../constants/images'
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import { openSettings, check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';


export default class DriverItem extends Component {

    constructor() {
        super()
    }

    callOnNumber = (number) => {
        if(Platform.OS == 'ios'){
            RNImmediatePhoneCall.immediatePhoneCall(number)
        }else{
            request(PERMISSIONS.ANDROID.CALL_PHONE).then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        alert('This feature is not available on this device')
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                        );
    
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        RNImmediatePhoneCall.immediatePhoneCall(number)
                        break;
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        this.showSettingsAlert();
                        // openSettings().catch(() => console.warn('cannot open settings'))
                        break;
                }
            })
                .catch(error => {
    
                });
        }

        

    }


    showSettingsAlert = () => {
        Alert.alert("Permission", "To make phone call, allow TaxiyeFood Restaruant App access to your phone call. Tap Settings > Permissions, and turn Phone on?",
            [
                {
                    text: "OK",
                    onPress: () => { openSettings().catch(() => console.warn('cannot open settings')) }
                },
                {
                    text: "Cancel",
                    onPress: () => { }
                },
            ])
    }


    render() {

        const { data } = this.props
        let name = "NA";
        let profileImage = Images.userDefaultImage;
        let contactNumber = "NA";
        if (!data.driver) {
            return null
        } else {
            console.log("DRIVER:" + JSON.stringify(data))
            if (data.driver.first_name && data.driver.last_name) {
                name = data.driver.first_name + " " + data.driver.last_name
            } else if (!data.driver.first_name && !data.driver.last_name) {
                name = "NA"
            } else if (!data.driver.first_name && data.driver.last_name) {
                name = data.driver.last_name
            } else if (data.driver.first_name && !data.driver.last_name) {
                name = data.driver.first_name
            }
            if (data.driver.profile_image && data.driver.profile_image != '') {
                profileImage = { uri: data.driver.profile_image };
            } else {
                profileImage = Images.userDefaultImage
            }

            if (data.driver.contact_number && data.driver.contact_number != '') {
                contactNumber = data.driver.contact_number
            } else {
                contactNumber = "NA"
            }

        }
        return (
            <View>
                <View style={styles.DividerHorizontal}></View>
                <Text style={[styles.DashboardItemCardProfileName, { paddingHorizontal: 8, paddingTop: 8 }]}>Driver Detail</Text>
               
                <View style={styles.DashboardItemCardProfileStyle}>
                    <View style={styles.DashboardItemCardProfileInnerStyle}>
                        <Image source={profileImage} style={styles.DriverImageStyle}></Image>
                        <View style={styles.DashboardItemCardProfileDetailView}>
                            <Text style={styles.DashboardItemCardProfileName}> {name}</Text>
                            <View style={styles.DashboardItemCardProfileContactView}>
                                <Image source={Images.mobile} style={styles.DashboardItemCardProfileContactIcon}></Image>
                                <Text style={styles.DashboardItemCardProfileContactDetail}>{contactNumber}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.DashboardItemCardProfileStatusViewStyle}>
                        {contactNumber != "NA" ? <TouchableOpacity style={styles.CallViewStyle} onPress={() => { this.callOnNumber(contactNumber) }}>
                            <Image source={Images.call} style={styles.CallImageStyle}></Image>
                        </TouchableOpacity>
                            : null}
                    </View>
                </View>
            </View>
        )
    }
}