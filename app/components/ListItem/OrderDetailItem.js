import React, { Component } from 'react'
import { Platform, StyleSheet, View, Text,Image } from 'react-native';
import styles from './styles'
import Images from '../../constants/images'
import Constants from '../../constants/constants'

class OrderDetailItem extends Component {

    constructor() {
        super()
    }

    renderADDONSText(addon) {
        return (
            <View style={{paddingLeft:15}}>
            {
                (addon && addon.length>0)?
                <Text style={[styles.DashboardItemOrderDetailTextStyle,{fontFamily:Constants.fontFamily.SemiBold}]}>ADDONS</Text>
                :null
            }
            </View>
        );
    }

    renderAddonItem(addon){
        if (!addon || addon.length ==0){
            return 
        } 
        for(let j = 0 ; j < addon.length; j++){
            return(
                
                <View style={[styles.OrderDetailContainerStyle]}>  
                   <View style={[styles.OrderDetailItemCommonStyle, { flexDirection:'row', alignItems: 'flex-start' }]}>
                        {/* <Image source={Images.location} style={styles.OrderDetailItemIcon}></Image> */}
                        <Text style={[styles.OrderDetailItemTextCommonStyle,{paddingLeft:20}]}>{addon[j].name}</Text>
                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle]}>
                        <Text style={[styles.OrderDetailItemTextCommonStyle]}>1</Text>
                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle]}>
                        <Text style={[styles.OrderDetailItemTextCommonStyle]}>{addon[j].price}</Text>
                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle]}>
                    </View>
                </View>
            )
        }
    }
    render() {
        let  itemData = this.props.value
        // console.log(itemData)
        return (            
            <View >
               
                <View style={styles.OrderDetailContainerStyle}>
                    <View style={[styles.OrderDetailItemCommonStyle, { flexDirection:'row', alignItems: 'flex-start' }]}>
                        <Image source={Images.location} style={styles.OrderDetailItemIcon}></Image>
                        <Text style={[styles.OrderDetailItemTextCommonStyle]}>{itemData.product_name}</Text>
                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle]}>
                        <Text style={[styles.OrderDetailItemTextCommonStyle]}>{itemData.product_quantity}</Text>

                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle]}>
                        <Text style={[styles.OrderDetailItemTextCommonStyle]}>{itemData.product_unit_price}</Text>

                    </View>
                    <View style={[styles.OrderDetailItemCommonStyle,{flexDirection: 'row-reverse'}]}>
                        <Text style={[styles.OrderDetailItemTextCommonStyle,]}>{itemData.product_total_price}</Text>
                        <Text style={[styles.OrderDetailItemTextCommonStyle,]}>{Constants.currency.rupees}</Text>
                    </View>

                </View>
                {this.renderADDONSText(itemData.addons_list)}

                
                {this.renderAddonItem(itemData.addons_list)}
        
                <View style={styles.DividerHorizontal}></View>

            </View>
        );
    };
}

export default OrderDetailItem