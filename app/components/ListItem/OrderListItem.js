import React, { Component } from 'react'
import { Platform, StyleSheet, View, Image, Text } from 'react-native';
import styles from './styles'
import Images from '../../constants/images'
import constants from '../../constants/constants'
import { TouchableOpacity } from 'react-native-gesture-handler';
import NavigationServices from '../../services/NavigationServices';
import moment from "moment";
import Constants from '../../constants/constants';

class OrderListItem extends Component {

    constructor() {
        super()
    }

    onItemClick(data){
        NavigationServices.navigate('OrderDetail', {jsonData:data})
    }
    
    getStatus = (status) => {
        let statusValue = ""
         Object.keys(Constants.ORDERS_STATUS).map((key) => {   
            const value = Constants.ORDERS_STATUS[key];
            if(status == value){
                //console.log(key)
                statusValue = key
            }
         })
    
         return statusValue;
        }
    // statusGet(status){
    //     if (status == 0){
    //         status = Constants.ORDER_STATUS.NoConfirm_0 
    //           return (status)
    //     }
    //      else if (status == 1){
    //        status = Constants.ORDER_STATUS.Pending_1
    //           return (status)
    //      }
    //      else if (status == 2){
    //         status = Constants.ORDER_STATUS.Drivers_2
    //            return (status)
    //       }
    //       else if (status == 3){
    //         status = Constants.ORDER_STATUS.Accepted_3
    //            return (status)
    //       }
    //       else if (status == 4){
    //         status = Constants.ORDER_STATUS.Dispatched_4
    //            return (status)
    //       }
    //       else if (status == 5){
    //         status = Constants.ORDER_STATUS.Delivered_5
    //            return (status)
    //       }
    //       else if (status == 6){
    //         status = Constants.ORDER_STATUS.Cancelled_6
    //            return (status)
    //       }
    //       else if (status == 7){
    //         status = Constants.ORDER_STATUS.Confirmed_7
    //            return (status)
    //       }
    // }
    render() {
        let  data = this.props.value
        let customerDetail = JSON.stringify(data.customer)
        let  customerData = JSON.parse(customerDetail)
 //  { <----------- Profile Image ------> }
        let Image_Http_URL;
        if (customerData && customerData.profile_image){
        Image_Http_URL = {uri: customerData.profile_image };
       // console.log(customerData.profile_image)     
        }
       else{
        Image_Http_URL = Images.userDefaultImage
       // console.log("-----------------")
       }
    //  { <----------- Shipping Address  ------> }
       let address = JSON.parse(data.ship_json) 
        if(address != null){
            let house_address = JSON.parse(address.address_json)
                if (house_address != null){
                    //console.log(add)
                    data.address = house_address.house_no + " " + house_address.street + " " + house_address.area_name + " " + house_address.city + " "+ house_address.state_id
                } 
                else{
                    data.address = "Not Available";
                    //console.log("----------------------------------------")
                }    
        }
        else{
            data.address = "Not Available";
            //console.log("----------------------------------------")
        }
    //  { <-----------  Delivery   Time ------> }
        // let dateTime = new Date(data.date + " " + data.time);
       let PlacedDateTime = moment(data.date + " " + data.time).format("ddd DD MMM, h:mm A"); 
       
       return (
            <TouchableOpacity onPress={()=>this.onItemClick(data)} style={styles.ContainerStyle}>
                <View style={styles.CardHeaderStyle}>
                    <View style={styles.CardHeaderView}>
                        <Text allowFontScaling={false} style={styles.CardHeaderTextNameStyle} >Placed On </Text>
                        <Text allowFontScaling={false} style={styles.CardHeaderTextValueStyle}>{PlacedDateTime}</Text>
                    </View>
                    <View style={styles.CardHeaderView}>
                        {/* <Text style={styles.CardHeaderTextNameStyle}>Delivered On </Text>
                        <Text style={styles.CardHeaderTextValueStyle}>{ data.time}</Text> */}
                    </View>  
                    {/* Tue 31 Jan, 9:02AM */}
                </View>
                <View style={styles.CardBottomStyle}>
                    {/* <Image style={styles.userImage} source={Images.userDefaultImage}></Image>
                    <Text>adfssdfsdfd</Text> */}
                    <View style={styles.CardProfileStyle}>
                        <Image   source = { Image_Http_URL } style={styles.CardProfileImageStyle}></Image>
                        <View style={styles.CardProfileDetailView}>
                            <Text style={styles.CardProfileName}>{this.props.value.key}</Text>
                            <View style={styles.CardProfileLocationView}>
                                <Image source={Images.location} style={styles.CardProfileLocationIcon}></Image>
                                <Text style={styles.CardProfileLocationAddress}>{data.address}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.CardOrderStyle}>
                        <View style={styles.CardOrderView}>
                            <Text style={styles.CardOrderTextLabel}>Order ID</Text>
                            <Text style={styles.CardOrderTextValue}>{data.id}</Text>
                        </View>
                        <View style={styles.CardOrderDivider}></View>
                        <View style={styles.CardOrderView}>
                            <Text style={styles.CardOrderTextLabel}>Order Status</Text>
                            <Text style={styles.CardOrderTextValue}>{this.getStatus(data.status)}</Text>
                        </View>
                        <View style={styles.CardOrderDivider}></View>
                        <View style={styles.CardOrderView}>
                            <Text style={styles.CardOrderTextLabel}>Amount</Text>
                            <View style={styles.CardOrderAmountView}>
                                <Text style={styles.CardOrderTextCurrency}>$ </Text>
                                <Text style={styles.CardOrderTextValue}>{data.amount}</Text>
                            </View>

                        </View>
                    </View>
                </View>

            </TouchableOpacity>
        );
    };
}

export default OrderListItem