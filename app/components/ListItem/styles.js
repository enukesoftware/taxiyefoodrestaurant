import { StyleSheet, Dimensions } from 'react-native'
import Constants from '../../constants/constants'
// const { width, fontScale } = Dimensions.get("window");

// const styles = StyleSheet.create({ fontSize: idleFontSize / fontScale, });

export default StyleSheet.create({

    /*------------------ OrderListItem Start --------------*/

    ContainerStyle: {
        margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5

    },
    CardHeaderStyle: {
        backgroundColor: Constants.color.cardHeaderColor,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 5,
        paddingBottom: 5,
    },
    CardHeaderView: {
        width: '50%',
        padding: 5,
        flexDirection: 'row',
    },

    CardHeaderTextNameStyle: {
        color: Constants.color.fieldNameColor,
        fontSize: Constants.fontSize.SmallX,
    },

    CardHeaderTextValueStyle: {
        width: '70%',
        color: Constants.color.fieldValueColor,
        fontSize: Constants.fontSize.SmallX,
        fontWeight: 'bold',
        flexWrap: 'wrap',

    },
    CardBottomStyle: {
        padding: 8,
        backgroundColor: 'white',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,

    },
    CardProfileStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    CardProfileImageStyle: {
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        backgroundColor: Constants.color.lightPrimary
    },
    CardProfileDetailView: {
        padding: 5,
    },
    CardProfileName: {
        width: '85%',
        color: Constants.color.fieldValueColor,
        fontSize: Constants.fontSize.SmallXXX,
        flexWrap: 'wrap',
    },
    CardProfileLocationView: {
        flexDirection: 'row',
        marginTop: 5,
        alignItems: 'center',
    },
    CardProfileLocationIcon: {
        width: 12,
        height: 12,
        marginRight: 5,
        tintColor:Constants.color.primary
    },
    CardProfileLocationAddress: {
        width: '85%',
        color: Constants.color.fieldNameColor,
        fontSize: Constants.fontSize.SmallXX,
        flexWrap: 'wrap',
    },
    CardOrderStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 15,
        marginBottom: 5,
    },
    CardOrderDivider: {
        width: 1,
        backgroundColor: Constants.color.defaultUnderLineColor,
    },
    CardOrderView: {
        alignItems: 'center',
        flex: 0.4,
        padding: 2,
    },
    CardOrderTextLabel: {
        color: Constants.color.fieldNameColor,
        fontSize: Constants.fontSize.SmallXX,
    },
    CardOrderTextValue: {
        marginTop: 5,
        fontWeight: 'bold',
        fontSize: Constants.fontSize.SmallXX,
        color: Constants.color.fieldValueColor,
    },
    CardOrderTextCurrency: {
        marginTop: 5,
        fontWeight: 'bold',
        fontSize: Constants.fontSize.SmallXX,
        color: Constants.color.primary,
    },
    CardOrderAmountView: {
        flexDirection: 'row',
    },

    /*------------------ OrderListItem End --------------*/


    /*------------------ OrderDetailItem Start --------------*/

    DividerHorizontal: {
        height: 1,
        backgroundColor: Constants.color.defaultUnderLineColor,
    },
    OrderDetailContainerStyle: {
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    OrderDetailItemCommonStyle: {
        flex: 1,
        alignItems: 'center',
    },
    OrderDetailItemTextCommonStyle: {
        fontSize: Constants.fontSize.SmallX,
        color: Constants.color.fieldValueColor,
    },
    OrderDetailItemIcon: {
        width: 12,
        height: 12,
        marginRight: 5,
        tintColor:Constants.color.primary
    },

    /*------------------ OrderDetailItem End --------------*/

    /*------------------ DashboardOrderItem Start --------------*/

    DashboardItemCardContainerStyle: {
        margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5,
        // height:200,
    },
    DashboardItemCardHeaderStyle: {
        backgroundColor: Constants.color.cardHeaderColor,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 5,
        paddingBottom: 5,
    },
    DashboardItemCardHeaderView: {
        width: '50%',
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
    },

    DashboardItemCardHeaderViewSingleDate: {
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
    },

    DashboardItemCardHeaderTextNameStyle: {
        color: Constants.color.fieldValueColor,
        fontSize: Constants.fontSize.SmallX,
        fontFamily:Constants.fontFamily.Regular,
    },

    DashboardItemCardHeaderTextValueStyle: {
        width: '65%',
        color: Constants.color.fieldValueColor,
        fontSize: Constants.fontSize.SmallX,
        // fontWeight: 'bold',
        flexWrap: 'wrap',
        fontFamily:Constants.fontFamily.SemiBold,
    },

    DashboardItemCardProfileStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
    },
    DashboardItemCardProfileInnerStyle: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    DashboardItemCardProfileImageStyle: {
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        backgroundColor: Constants.color.lightPrimary
    },
    DashboardItemCardProfileDetailView: {
        padding: 5,
        width: '70%',
    },
    DashboardItemCardProfileName: {
        color: Constants.color.fieldValueColor,
        fontSize: Constants.fontSize.SmallXX,
        flexWrap: 'wrap',
        fontFamily:Constants.fontFamily.SemiBold,
    },
    DashboardItemCardProfileContactView: {
        flexDirection: 'row',
        marginTop: 5,
        alignItems: 'center',
    },
    DashboardItemCardProfileContactIcon: {
        width: 12,
        height: 12,
        marginRight: 5,
        tintColor:Constants.color.primary
    },
    DashboardItemCardProfileContactDetail: {
        color: Constants.color.fieldNameColor,
        fontSize: Constants.fontSize.SmallX,
        flexWrap: 'wrap',
        fontFamily:Constants.fontFamily.Regular,
    },
    DashboardItemCardProfileStatusViewStyle: {
        width: '30%',
        alignItems: 'flex-end',
    },
    DashboardItemCardProfileStatusStyle: {
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        backgroundColor: Constants.color.cardHeaderColor,
    },
    DashboardItemCardProfileStatusTextStyle: {
        color: Constants.color.fieldValueColor,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        // fontWeight: 'bold',
        fontSize: Constants.fontSize.SmallX,
        fontFamily:Constants.fontFamily.SemiBold,
    },
    DashboardItemOrderDetailTextStyle: {
        paddingHorizontal: 10,
        paddingVertical:5,
        fontSize: Constants.fontSize.SmallX,
        color: Constants.color.fieldValueColor,
        // fontFamily:Constants.fontFamily.Medium,
    },
    DividerVertical: {
        width: 1,
        backgroundColor: Constants.color.defaultUnderLineColor,
    },
    DividerHorizontal: {
        height: 1,
        backgroundColor: Constants.color.defaultUnderLineColor,
    },
    // DashboardItemCardLocationView: {
    //     flexDirection: 'row',
    //     padding: 10,
    //     alignItems: 'center',
    // },
    // DashboardItemCardLocationIcon: {
    //     width: 12,
    //     height: 12,
    //     marginRight: 5,
    // },
    // DashboardItemCardLocationAddress: {
    //     width: '90%',
    //     color: Constants.color.fieldNameColor,
    //     fontSize: Constants.fontSize.SmallXX,
    //     flexWrap: 'wrap',
    // },
    DashboardBottomViewNeworder: {
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center',
        padding:15,
    },

    DasboardBottomNewOrderTouchableDecline:{
        borderRadius:5,
        backgroundColor:Constants.color.primary,
        paddingVertical:5,
        paddingHorizontal:15,
        marginHorizontal:5,
        borderColor:Constants.color.primary,
        borderWidth:1,
    },
    DasboardBottomNewOrderTouchableAccept:{
        borderRadius:5,
        borderColor:Constants.color.defaultUnderLineColor,
        borderWidth:1,
        paddingVertical:5,
        paddingHorizontal:15,
        marginHorizontal:5,
    },
    DasboardBottomNewOrderTextCommon:{
        fontSize:Constants.fontSize.SmallXX,
        fontFamily:Constants.fontFamily.Bold

    },
    DashboardBottomViewCompleteOrder:{
        flexDirection:'row',
        padding:10,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent:'center',
        alignItems:'center'
    },

    DashboardBottomCompleteOrderTextCommon:{
        fontSize:Constants.fontSize.SmallX,
        color: Constants.color.fieldValueColor
    },
    DashboardBottomViewPendingOrder:{
        flexDirection:'row',
        paddingHorizontal:10,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent:'center',
        alignItems:'center'
    },

    DropDownTouchableStyle:{
        height:40,
        flex:1,
        alignItems:'flex-end',
    },
    DropDownInnerViewStyle:{
        flexDirection: 'row',
        alignItems:'center',
        // position:'absolute'
    },
    DropDownArrowStyle: {
        paddingVertical: 5,
        marginVertical: 5,
        height: 12,
        width: 12,
        resizeMode: 'stretch',
        alignItems: 'center',
        position:'absolute',
        right:'1%',
    },
    DriverImageStyle: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: Constants.color.lightPrimary
    },
    CallViewStyle:{
        height:40,
        width:40,
        borderRadius:20, 
        backgroundColor: Constants.color.lightPrimary,
        alignItems:'center',
        justifyContent:'center',
        marginRight:8
    },
    CallImageStyle: {
        height: 15,
        width: 15,
        tintColor:Constants.color.primary
    },

    /*------------------ DashboardOrderItem End --------------*/
})

