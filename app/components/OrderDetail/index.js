import React, { Component } from 'react'
import { Platform, StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import styles from './styles'
import Constants from '../../constants/constants'
import Images from '../../constants/images'
import OrderDetailItem from '../ListItem/OrderDetailItem'
import moment from "moment";
import DriverItem from '../ListItem/DriverItem'

class OrderDetail extends Component {

    static navigationOptions = {
        title: 'Order Detail',
        headerStyle: {
            backgroundColor: Constants.color.primary,
        },
        headerTitleStyle: {
            color: 'white',
            fontWeight: 'bold'
        },
        headerTintColor : 'white',
    };

    constructor() {
        super()
        this.data = [
            { key: 'Dosa' },
            { key: 'Shake' },
            { key: 'Biryani' },
            { key: 'Poha' },
            { key: 'Cake' },
            { key: 'Sandwich' },
        ]
    }
    getStatus = (status) => {
        let statusValue = ""
         Object.keys(Constants.ORDERS_STATUS).map((key) => {   
            const value = Constants.ORDERS_STATUS[key];
            if(status == value){
                console.log(key)
                statusValue = key
            }
         })
    
         return statusValue;
        }
    // statusGet(status){
    //     if (status == 0){
    //         status = Constants.ORDER_STATUS.NoConfirm_0 
    //           return (status)
    //     }
    //      else if (status == 1){
    //        status = Constants.ORDER_STATUS.Pending_1
    //           return (status)
    //      }
    //      else if (status == 2){
    //         status = Constants.ORDER_STATUS.Drivers_2
    //            return (status)
    //       }
    //       else if (status == 3){
    //         status = Constants.ORDER_STATUS.Accepted_3
    //            return (status)
    //       }
    //       else if (status == 4){
    //         status = Constants.ORDER_STATUS.Dispatched_4
    //            return (status)
    //       }
    //       else if (status == 5){
    //         status = Constants.ORDER_STATUS.Delivered_5
    //            return (status)
    //       }
    //       else if (status == 6){
    //         status = Constants.ORDER_STATUS.Cancelled_6
    //            return (status)
    //       }
    //       else if (status == 7){
    //         status = Constants.ORDER_STATUS.Confirmed_7
    //            return (status)
    //       }
    // }
    render() {
        const data = this.props.navigation.getParam("jsonData");
        let data_address = ""
       // console.log(data) 
    //    let dateTime = new Date(data.date + " " + data.time);
       let PlacedDateTime = moment(data.date + " " + data.time).format("ddd DD MMM, h:mm A"); 
       let customerDetail = JSON.stringify(data.customer)
       let  customerData = JSON.parse(customerDetail)
       name = customerData.first_name + " " + customerData.last_name
      // console.log(name)
    //    for (i in data.items) {
    //     itemData = data.items[i]
    //   }
      // console.log(data.item)
        let driverDetail =  JSON.stringify(data.driver)
       let driverData =  JSON.parse(driverDetail)
       if (driverData != null ){
           driverName = driverData.first_name + " " + driverData.last_name
       }
       else{
        driverName = "NA"
       }
          //console.log(driverData)
     //  {<----------- Shipping Address  ------> }
       let address = JSON.parse(data.ship_json) 
       if(address != null){
           let house_address = JSON.parse(address.address_json)
               if (house_address != null){
                   //console.log(add)
                   data_address = house_address.house_no + " " + house_address.street + " " + house_address.area_name + " " + house_address.city + " "+ house_address.state_id
               } 
               else{
                   data_address = "Not Available";
                   //console.log("----------------------------------------")
               }    
       }
       else{
        data_address = "Not Available";
        //console.log("----------------------------------------")
    }
       //  { <----------- Profile Image ------> }
       let Image_Http_URL;
       if (customerData && customerData.profile_image){
       Image_Http_URL = {uri: customerData.profile_image };
       //console.log(customerData.profile_image)     
       }
      else{
       Image_Http_URL = Images.userDefaultImage
       //console.log("-----------------")
      }
        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.pageBackgroundColor }}>
                <ScrollView>
                    {/* Upper Card */}
                    <View style={styles.CardContainerStyle}>

                        {/* Header */}
                        <View style={styles.CardHeaderStyle}>
                            <View style={[styles.CardHeaderView, { paddingLeft: 5 }]}>
                                <Text allowFontScaling={false} style={styles.CardHeaderTextNameStyle} >Placed On </Text>
                                <Text allowFontScaling={false} style={styles.CardHeaderTextValueStyle}>{PlacedDateTime}</Text>
                            </View>
                            {/* <View style={[styles.CardHeaderView, { paddingLeft: 5, alignSelf: 'flex-end' }]}>
                                <Text style={styles.CardHeaderTextNameStyle}>Delivered On </Text>
                                <Text style={styles.CardHeaderTextValueStyle}>Tue 31 Jan, 9:02AM</Text>
                            </View> */}
                        </View>

                        {/* Profile Detail */}
                        <View style={styles.CardProfileStyle}>
                            <View style={styles.CardProfileInnerStyle}>
                                <Image  source = {Image_Http_URL} style={styles.CardProfileImageStyle}></Image>
                                <View style={styles.CardProfileDetailView}>
                                    <Text style={styles.CardProfileName}>{name}</Text>
                                    <View style={styles.CardProfileContactView}>
                                        <Image source={Images.mobile} style={styles.CardProfileContactIcon}></Image>
                                        <Text style={styles.CardProfileContactDetail}>{customerData.contact_number}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.CardProfileStatusViewStyle}>
                                <View style={styles.CardProfileStatusStyle}>
                                    <Text style={styles.CardProfileStatusTextStyle}>{this.getStatus(data.status)}</Text>
                                </View>
                            </View>
                        </View>

                        <Text style={styles.OrderDetailTextStyle}>ORDER DETAILS</Text>
                        <View style={styles.DividerHorizontal}></View>

                        {/* Middle View */}
                        <View style={styles.CardOrderStyle}>
                            <View style={styles.CardOrderView}>
                                <Text style={styles.CardOrderTextLabel}>Order ID</Text>
                                <Text style={styles.CardOrderTextValue}>{data.items[0].order_id}</Text>
                            </View>
                            <View style={styles.CardOrderDivider}></View>
                            <View style={styles.CardOrderView}>
                                <Text style={styles.CardOrderTextLabel}>Payment Method</Text>
                                <Text style={styles.CardOrderTextValue}>{data.user_payment_method}</Text>
                            </View>
                            <View style={styles.CardOrderDivider}></View>
                            <View style={styles.CardOrderView}>
                                <Text style={styles.CardOrderTextLabel}>Driver</Text>
                                <View style={styles.CardOrderAmountView}>
                                    {/* <Text style={styles.CardOrderTextCurrency}>$ </Text> */}
                                    <Text style={styles.CardOrderTextValue}>{driverName}</Text>
                                </View>

                            </View>
                        </View>

                        <View style={styles.DividerHorizontal}></View>
                        <Text style={styles.OrderDetailTextStyle}>DELIVERY ADDRESS</Text>
                        <View style={styles.DividerHorizontal}></View>

                        <View style={styles.CardLocationView}>
                            <Image source={Images.location} style={styles.CardLocationIcon}></Image>
                            <Text style={styles.CardLocationAddress}>{data_address}</Text>
                        </View>
                        <DriverItem data={data}></DriverItem>

                    </View>

                    {/* Bottom Card */}
                    <View style={styles.CardContainerStyle}>
                        {/* Header View */}
                        <View style={styles.CardBottomHeader}>
                            <Text style={[styles.BottomHeaderTextStyle, { textAlign: 'left' }]}>ITEMS</Text>
                            <Text style={[styles.BottomHeaderTextStyle,]}>QTY</Text>
                            <Text style={[styles.BottomHeaderTextStyle,]}>UNIT PRICE</Text>
                            <Text style={[styles.BottomHeaderTextStyle, { textAlign: 'right' }]}>PRICE</Text>
                        </View>
                        <View style={styles.DividerHorizontal}></View>
                        {/* <ScrollView> */}
                            {data.items.map((item, key) => (
                                // <Text key={key} > {item.key} </Text>
                                <OrderDetailItem
                                    key={key}
                                    value={item}   
                                />
                            )
                            )}
                        {/* </ScrollView> */}
                        {/* Total View */}
                            <View style={styles.SubTotalViewStyle}>
                                <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Discount</Text>
                                <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                    <Text style={[styles.CommonTextValueStyle,]}>{data.discount}</Text>
                                    <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                                </View>
                            </View>
                            <View style={styles.SubTotalViewStyle}>
                                <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Shipping Charge</Text>
                                <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                    <Text style={[styles.CommonTextValueStyle,]}>{data.shipping_charge}</Text>
                                    <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                                </View>
                            </View>
                        {/* <View style={styles.SubTotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Commission</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.commission}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View> */}

                        <View style={styles.SubTotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Shipping Charge</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.shipping_charge}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View>

                        <View style={styles.SubTotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Packaging fees</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.packaging_fees}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View>

                        <View style={styles.SubTotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>CGST</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.cgst}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View>

                        <View style={styles.SubTotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>SGST</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.sgst}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View>

                        <View style={styles.DividerHorizontal}></View>
    
                        <View style={styles.TotalViewStyle}>
                            <Text style={[styles.CommonTextValueStyle, { width: '50%' }]}>Total</Text>
                            <View style={{ width: '50%', flexDirection: 'row-reverse', }}>
                                <Text style={[styles.CommonTextValueStyle,]}>{data.amount}</Text>
                                <Text style={[styles.CommonTextValueStyle,]}>{Constants.currency.rupees}</Text>
                            </View>

                        </View>

                    </View>

                </ScrollView>
            </View>
        );
    };
}

export default OrderDetail