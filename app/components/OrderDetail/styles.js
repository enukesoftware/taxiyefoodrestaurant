import { StyleSheet } from 'react-native';
import Constants from './../../constants/constants'


export default StyleSheet.create({
    CardContainerStyle:{
        margin: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5,
        // height:200,
    },
    CardHeaderStyle: {
        backgroundColor: Constants.color.cardHeaderColor,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop:5,
        paddingBottom:5,
    },
    CardHeaderView: {
        width:'50%',
        paddingTop:5,
        paddingBottom:5,
        flexDirection: 'row',
    },
   
    CardHeaderTextNameStyle:{
        color:Constants.color.fieldNameColor,
        fontSize:Constants.fontSize.SmallX,
    },

    CardHeaderTextValueStyle:{
        width:'70%',
        color:Constants.color.fieldValueColor,
        fontSize:Constants.fontSize.SmallX,
        fontWeight: 'bold',
        flexWrap:'wrap',
       
    },

    CardProfileStyle:{
        flexDirection:'row',
        alignItems:'center',
        padding:8,
    },
    CardProfileInnerStyle:{
        width:'70%',
        flexDirection:'row',
        alignItems:'center',
    },
    CardProfileImageStyle:{
        height:60,
        width:60,
        borderRadius:60/2,
        backgroundColor:'grey'
    },
    CardProfileDetailView:{
        padding:5,
        width:'70%',
    },
    CardProfileName:{
        color:Constants.color.fieldValueColor,
        fontSize:Constants.fontSize.SmallXXX,
        flexWrap:'wrap',
    },
    CardProfileContactView:{
        flexDirection:'row',
        marginTop:5,
        alignItems:'center',
    },
    CardProfileContactIcon:{
        width:12,
        height:12,
        marginRight:5,
        tintColor:Constants.color.primary
    },
    CardProfileContactDetail:{
        color:Constants.color.fieldNameColor,
        fontSize:Constants.fontSize.SmallXX,
        flexWrap: 'wrap',
    },
    CardProfileStatusViewStyle:{
        width:'30%',
        alignItems:'flex-end',
    },
    CardProfileStatusStyle:{
        borderRadius:5,
        backgroundColor:Constants.color.yellow,
    },
    CardProfileStatusTextStyle:{
        color:Constants.color.fieldValueColor,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:15,
        paddingRight:15,
        fontWeight:'bold',
        fontSize:Constants.fontSize.SmallX,
    },
    OrderDetailTextStyle:{
        padding:10,
        fontSize:Constants.fontSize.SmallXX,
        color:Constants.color.fieldValueColor,
    },
    DividerVertical:{
        width:1,
        backgroundColor:Constants.color.defaultUnderLineColor,
    },
    DividerHorizontal:{
        height:1,
        backgroundColor:Constants.color.defaultUnderLineColor,
    },
    CardLocationView:{
        flexDirection:'row',
        padding:10,
        alignItems:'center',
    },
    CardLocationIcon:{
        width:12,
        height:12,
        marginRight:5,
        tintColor:Constants.color.primary
    },
    CardLocationAddress:{
        width:'90%',
        color:Constants.color.fieldNameColor,
        fontSize:Constants.fontSize.SmallXX,
        flexWrap: 'wrap',
    },
    CardOrderStyle:{
        flexDirection:'row',
        justifyContent:'space-around',
        marginTop:10,
        marginBottom:10,
    },
    CardOrderDivider:{
        width:1,
        backgroundColor:Constants.color.defaultUnderLineColor,
    },
    CardOrderView:{
        alignItems:'center',
        flex:0.4,
        padding:2,
    },
    CardOrderTextLabel:{
        color:Constants.color.fieldNameColor,
        fontSize:Constants.fontSize.SmallXX,
    },
    CardOrderTextValue:{
        marginTop:5,
        fontWeight:'bold',
        fontSize:Constants.fontSize.SmallXX,
        color:Constants.color.fieldValueColor,
    },
    CardOrderTextCurrency:{
        marginTop:5,
        fontWeight:'bold',
        fontSize:Constants.fontSize.SmallXX,
        color:Constants.color.primary,
    },
    CardOrderAmountView:{
        flexDirection:'row',
    },

    // Bottom Card
    CardBottomHeader:{
        // backgroundColor: Constants.color.cardHeaderColor,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-around',
        padding:10,
    },
    BottomHeaderTextStyle:{
        fontSize:Constants.fontSize.SmallX,
        color:Constants.color.fieldValueColor,
        flex:1,
        alignSelf:'center',
        textAlign:'center',
        margin:2,
    },
    CommonTextValueStyle:{
        fontSize:Constants.fontSize.SmallX,
        color:Constants.color.fieldValueColor,
    },
    SubTotalViewStyle:{
        flexDirection:'row',
        padding:10,
        paddingVertical:5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    TotalViewStyle:{
        flexDirection:'row',
        padding:10,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    }
})