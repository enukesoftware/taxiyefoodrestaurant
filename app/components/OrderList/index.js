import React, { Component } from 'react'
import { Platform, StyleSheet, View, ArrayMa, Text, ScrollView } from 'react-native';
import styles from './styles'
import OrderListItem from '../ListItem/OrderListItem'
import Constants from '../../constants/constants'

class OrderList extends Component {

    static navigationOptions = {
        title: 'Orders',
        headerStyle: {
            backgroundColor: Constants.color.primary,
        },
        headerTitleStyle: {
            color: 'white',
            fontWeight: 'bold'
        },
        headerTintColor : 'white',
    };


    constructor() {
        super()
        this.data = [
            { key: 'Devin' },
            { key: 'Jackson' },
            { key: 'James' },
            { key: 'Joel' },
            { key: 'John' },
            { key: 'Jillian' },
            { key: 'Jimmy' },
            { key: 'Devin' },
            { key: 'Jackson' },
            { key: 'James' },
            { key: 'Joel' },
            { key: 'John' },
            { key: 'Jillian' },
            { key: 'Jimmy' }
        ]

        this.SampleNameArray = ["Pankaj", "Rita", "Mohan", "Amit", "Babulal", "Sakshi"];


    }

    render() {
        const orderData = this.props.navigation.getParam("jsonData");
        console.log("data:",orderData.data)

        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.pageBackgroundColor }}>
                {/* <OrderListItem /> */}
                {/* <FlatList
                    data={this.d}
                    renderItem={({ item }) =>
                        <OrderListItem
                            value={item}
                        />
                    }
                /> */}
                <ScrollView>
                    {orderData.data.map((item, key) => (
                        // <Text key={key} > {item.key} </Text>
                        <OrderListItem
                            key={key}
                            value={item}
                        />
                    )
                    )}
                </ScrollView>
            </View>
        );

        // var SampleNameArray = ["Pankaj", "Rita", "Mohan", "Amit", "Babulal", "Sakshi"];

        // return (
        //     <View>

        //         {this.d.map((item, key) => (
        //             <Text key={key} > {item.key} </Text>)
        //         )}

        //     </View>
        // );
    };
}

export default OrderList