import React, { Component } from 'react'
import {
    Platform,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    Image,
    NetInfo,
} from 'react-native';
import styles from './styles'
// import DatePicker from 'react-native-datepicker';
import moment from "moment";
import { Dropdown } from 'react-native-material-dropdown';
import TextInput from 'react-native-material-textinput'
import * as ApiManager from './../../services/ApiManager'
import AlertManager from './../../constants/alert'
import Constants from '../../constants/constants'
import CustomText from '../custom/CustomText'
import Images from '../../constants/images'
import CustomDatePickerView from '../custom/CustomDatePickerView'
import NavigationServices from '../../services/NavigationServices';
import AsyncStorage from '@react-native-community/async-storage';

class SearchOrder extends Component {


    static navigationOptions = {
        title: 'Search Order',
        headerStyle: {
            backgroundColor: Constants.color.primary,
        },
        headerTitleStyle: {
            color: 'white',
            fontWeight: 'bold'
        },
        headerTintColor: 'white',


    };
    orderStatus = [
        { key: "0", value: "NoConfirm", selected: false },
        { key: "1", value: "Pending", selected: false },
        { key: "2", value: "Drivers", selected: false },
        { key: "3", value: "Accepted", selected: false },
        { key: "4", value: "Dispatched", selected: false },
        { key: "5", value: "Delivered", selected: false },
        { key: "6", value: "Cancelled", selected: false },
        { key: "7", value: "Confirmed", selected: false },
    ]

    constructor() {
        super()
        this.state = {
            loading: false,
            // currentDate: moment(new Date()).format("YYYY-MM-DD"),
            currentDate: new Date(),
            fromDate: '',
            toDate: '',
            selectedStatus: { key: "", value: "", selected: false },
            orderId: '',
            fromDateAsDate: new Date(),
            toDateAsDate: new Date(),
            userData: null,
        }

    }

    componentDidMount() {
        AsyncStorage.getItem('userData').then((res) => {
            if (res) {
                this.setState({
                    userData: JSON.parse(res)
                })
                console.log("USER_DATA:" + JSON.stringify(JSON.parse(res)))
            }
        }).catch((err) => {
            console.log("ERROR:" + JSON.stringify(err))
        })
    }

    onTextChange = key => value => {
        this.setState({
            [key]: value
        })
    }

    onDateSelect = key => value => {
        let convertedDate = moment(value).format('YYYY-MM-DD')
        switch (key) {
            case 'fromDate':
                // this.fromDatePicker.onPressDate()
                this.setState({
                    [key]: convertedDate,
                    fromDateAsDate: new Date(value)
                })

                if (this.state.toDate != '' && convertedDate > this.state.toDate) {
                    this.setState({
                        toDate: moment(new Date()).format("YYYY-MM-DD"),
                        toDateAsDate: new Date()
                    })
                }

                break
            case 'toDate':
                // this.toDatePicker.onPressDate()
                this.setState({
                    [key]: convertedDate,
                    toDateAsDate: new Date(value)
                })
                break
            // case 'status':

            //     break
        }
    }

    openDropDown() {
        this.dropdown.focus()
    }

    searchPressed1() {
        NavigationServices.navigate('OrderList')
    }

    searchPressed() {
        //  NetInfo.isConnected.fetch().then(isConnected => {
        //     console.log('First, is ' + (isConnected ? 'online' : 'offline'));
        //     if (!isConnected) {
        //         AlertManager.showAlert('No Internet Connetion', 'Please connect to internet and try again.', 'ok')
        //         return
        //     }
        // });

        if (!this.isValidate()) {
            return
        }

        this.setState({
            loading: true,
        })

        let resId = this.state.userData.restaurent_details.restaurent_id
        let body = {
            restaurant_id: resId,
            date: "",
            from: this.state.fromDate,
            to: this.state.toDate,
            order_id: this.state.orderId,
            status: this.state.selectedStatus.key,
            limit: "",
            offset: "",
            sorting: " ",
            sort_field: "",
        }
        console.log(body)
        ApiManager.orderList(body)
            .then(res => {
                this.setState({
                    loading: false,
                })

                if (res.success) {
                    if (res.data != null) {
                        console.log(res.data)
                        AsyncStorage.setItem("isLogin", "true");
                        // console.warn(JSON.stringify(res.data))
                        //  AsyncStorage.setItem('userData', JSON.stringify(res.data));
                        //  this.props.navigation.navigate('Dashboard')
                        // const jsonData= JSON.stringify(res);
                        // console.log(jsonData);
                        this.props.navigation.navigate('OrderList', { jsonData: res.data })
                    }
                } else {
                    setTimeout(() => {
                        AlertManager.showAlert('Error', res.error, 'ok')
                    }, 500)
                }

                // console.warn(JSON.stringify(res))
            }).catch(err => {
                this.setState({
                    loading: false,
                })

                setTimeout(() => {
                    AlertManager.showAlert('Error', err, 'ok')
                }, 500)
            })
    }

    resetPressed() {

        console.log('DATA', this.state.selectedStatus);
        this.setState({
            fromDate: '',
            toDate: '',
            selectedStatus: { key: "", value: "", selected: false },
            orderId: '',
            fromDateAsDate: new Date(),
            toDateAsDate: new Date,
        })
    }

    isValidate() {
        // const {username, password} = this.state;
        if (this.state.fromDate == ''
            && this.state.toDate == ''
            && this.state.selectedStatus.value == ''
            && this.state.orderId == '') {
            AlertManager.showAlert('Alert', 'Please select search criteria', 'Ok')
            return false;
        }

        if (this.state.fromDate != '' && this.state.toDate == '') {
            AlertManager.showAlert('Alert', 'Please select to date', 'Ok')
            return false;
        } else if (this.state.fromDate == '' && this.state.toDate != '') {
            AlertManager.showAlert('Alert', 'Please select from date', 'Ok')
            return false;
        }

        return true;

    }

    renderPicker() {
        // if (this.state.showFromDatePicker) {
        //     return (
        //         <DatePicker
        //             ref={(picker) => { this.fromDatePicker = picker; }}
        //             style={styles.DatePickerStyle}
        //             date={this.state.fromDate} //initial date from state
        //             mode="date" //The enum of date, datetime and time
        //             placeholder="From Date"
        //             format="YYYY-MM-DD"
        //             minDate="01-01-2016"
        //             maxDate={this.state.currentDate}
        //             confirmBtnText="Confirm"
        //             cancelBtnText="Cancel"
        //         />
        //     );
        // }
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.loading}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <ActivityIndicator
                                animating={this.state.loading} />
                        </View>
                    </View>
                </Modal>


                <CustomDatePickerView
                    placeholder='From Date'
                    icon={Images.calendar}
                    date={this.state.fromDate} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    minDate={new Date("01/01/2016")}
                    maxDate={this.state.currentDate}
                    selectedDate={this.state.fromDateAsDate}
                    onDateSelect={this.onDateSelect('fromDate')}
                >
                </CustomDatePickerView>

                <CustomDatePickerView
                    container={styles.DatePickerStyle}
                    placeholder='To Date'
                    icon={Images.calendar}
                    date={this.state.toDate} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    minDate={this.state.fromDateAsDate}
                    maxDate={this.state.currentDate}
                    selectedDate={this.state.toDateAsDate}
                    onDateSelect={this.onDateSelect('toDate')}
                >

                </CustomDatePickerView>

                <TouchableOpacity onPress={() => this.openDropDown()} style={styles.DropDownTouchableStyle}>
                    <View style={styles.DropDownInnerViewStyle}>
                        <Image source={Images.status} style={styles.DropDownImageStyle} />
                        <Dropdown
                            ref={dropdown => this.dropdown = dropdown}
                            containerStyle={
                                styles.inputAndroid
                            }
                            fontSize={Constants.fontSize.SmallXXX}
                            dropdownOffset={{ top: 8, left: 0 }}
                            placeholderTextColor={Constants.color.defaultTextColor}
                            baseColor="rgba(255, 255, 255, 1)"
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            placeholder='Status'
                            data={this.orderStatus}
                            onChangeText={(value, index, data) => this.setState({ selectedStatus: this.orderStatus[index] })}
                            value={this.state.selectedStatus.value}
                        />
                        <Image source={Images.dropdown} style={styles.DropDownArrowStyle} />

                    </View>
                    <View style={styles.DropDownUnderlineStyle}></View>

                </TouchableOpacity>



                <View style={styles.textInputView}>

                    <CustomText container={styles.textInput}
                        placeholder='Order ID'
                        icon={Images.orderId}
                        onTextChange={this.onTextChange('orderId')}
                        value={this.state.orderId}
                    />
                </View>

                <TouchableOpacity onPress={() => this.searchPressed()} style={styles.touchable}>
                    <Text style={styles.btnText}>SEARCH</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.resetPressed()} style={styles.touchable}>
                    <Text style={styles.btnText}>RESET</Text>
                </TouchableOpacity>
            </View>
        );
    };
}



export default SearchOrder
