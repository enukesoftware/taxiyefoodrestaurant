import { StyleSheet } from 'react-native';
import Constants from './../../constants/constants'


export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        padding:20
    },
    DatePickerStyle: {
        marginTop: 10,
    },
    inputIOS: {
        flex: 1,
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        flex:1,
        paddingLeft:0,
        paddingRight:10,
        color: 'black',
        height:40,
    },
    textInputView:{
        // width:300,
        marginTop:10,
    },
    textInput: {
        fontSize: 20,
    },


    touchable: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        padding: 10,
        marginTop: 30,
        borderRadius:5,
        alignSelf:'center',
        width:'100%'
    },
    btnText: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        fontWeight: 'bold',
    },
    activityIndicatorWrapper:{
        backgroundColor: Constants.color.progressDialogColor,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        opacity: 0.5,
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: Constants.blue
    },
    DropDownTouchableStyle:{
        marginTop:10,
        height:40,
    },
    DropDownInnerViewStyle:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    DropDownImageStyle: {
        padding: 5,
        margin: 5,
        height: 16,
        width: 16,
        resizeMode: 'stretch',
        alignItems: 'center',
        tintColor:Constants.color.primary,
    },
    DropDownArrowStyle: {
        padding: 5,
        margin: 5,
        height: 12,
        width: 12,
        resizeMode: 'stretch',
        alignItems: 'center',
    },
    DropDownUnderlineStyle:{
        height:1,
        backgroundColor:Constants.color.defaultUnderLineColor,
    }
})