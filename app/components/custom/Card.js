import React, { Component } from 'react'
import { Platform, StyleSheet, View } from 'react-native';
import styles from './styles'
import { tsPropertySignature } from '@babel/types';

export const Card = (props) => {
    return (
        <View style={styles.cardStyle}>
            {props.children}
        </View>
    )
}