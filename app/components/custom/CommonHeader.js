import { Dimensions, Platform, View, TouchableOpacity, Text, Image, StyleSheet } from "react-native";
import React from "react";
import Images from "../../constants/images";
import Constants from "../../constants/constants";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import NavigationServices from "../../services/NavigationServices";

export default class CommonHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    onBackClick = () => {
        NavigationServices.goBack();
    }

    renderBackButton() {
        return (
            <TouchableOpacity activeOpacity={0.8}
                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}
                onPress={() => this.onBackClick()}>
                <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
                <Text style={styles.backBtnText}> Back</Text>
            </TouchableOpacity>
        )
    }

    renderToolbar() {
        return (
            <View style={this.props.isTransparent ? styles.transparentToolbar : styles.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '100%',
                    paddingHorizontal: 10
                }}>
                    <View style={{
                        width: '15%', alignItems: 'center', justifyContent: 'center',
                        //  backgroundColor: 'yellow',
                    }}>
                        {this.props.noBack ? null : this.renderBackButton()}
                    </View>
                    <View style={{
                        width: '70%', alignItems: 'center',
                        // backgroundColor: 'red',
                    }}>
                        {this.props.titleView ? this.props.titleView() :
                            <Text style={styles.headerTitleName} >{this.props.title}</Text>}
                    </View>
                    <View style={{ width: '15%' }}>
                        {this.props.rightImg ?
                            <TouchableOpacity style={styles.headerButton} onPress={() => this.props.fireEvent ? this.props.fireEvent() : {}}>
                                <Image source={this.props.rightImg ? this.props.rightImg : null} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
                            </TouchableOpacity> : null
                        }
                        {
                            this.props.twoMenu ?
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => this.props.firstMenuEvent ? this.props.firstMenuEvent() : {}}>
                                        <Image source={this.props.firstMenuImg ? this.props.firstMenuImg : null}
                                            style={{ marginRight: 15, height: 20, width: 20 }}
                                        ></Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.props.secondMenuEvent ? this.props.secondMenuEvent() : {}} >
                                        <Image source={this.props.secondMenuImg ? this.props.secondMenuImg : null}
                                            style={{ marginRight: 15, height: 20, width: 20 }}
                                        ></Image>
                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                    </View>
                </View>
            </View>
        )
    }

    render() {
        let { fireEvent,firstMenuEvent,secondMenuEvent } = this.props;
        return (
            this.renderToolbar()
        )
    }
}

const styles = StyleSheet.create({
    toolbar: {
        height: (Platform.OS === 'ios') ? 44 : 56,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 3,
        shadowRadius: 0,
    },
    transparentToolbar: {
        height: (Platform.OS === 'ios') ? 44 : 56,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
    },
    headerTitleName: {
        color: Constants.color.white,
        fontSize: 17,
        fontFamily: Constants.fontFamily.SemiBold,
    },
    backBtnText: {
        color: Constants.color.white,
        fontFamily: Constants.fontFamily.SemiBold,
    },
})