import React, { Component } from 'react'
import { Platform, StyleSheet, View, Image, Text,TouchableOpacity } from 'react-native';
import styles from './styles'
import Images from '../../constants/images'
import Constants from '../../constants/constants'
import DateTimePicker from "react-native-modal-datetime-picker";

class CustomDatePickerView extends Component {

    constructor() {
        super()
        this.state = {
            underLineColor: Constants.color.defaultUnderLineColor,
            isDateTimePickerVisible: false
        }
    }

    showDateTimePicker = () => {
        console.log("BUTTON CLICK")
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        // console.log("A date has been picked: ", date);
        this.hideDateTimePicker();
        this.props.onDateSelect(date.toString())
    };

    render() {
        return (
            <View style={[styles.container, this.props.container]}>
                <TouchableOpacity onPress={this.showDateTimePicker}>
                    <View style={styles.SectionStyle}>
                        <Image source={this.props.icon} style={styles.CustomDatePickerFirstImageStyle} />
                        <Text
                            style={styles.CustomDatePickerTextStyle}
                        >
                            {(this.props.date == undefined || this.props.date == '') ? this.props.placeholder : this.props.date}
                        </Text>
                        <Image source={Images.dropdown} style={styles.CustomDatePickerSecondImageStyle} />

                    </View>
                    <View style={[styles.UnderlineStyle, { backgroundColor: this.state.underLineColor }]}></View>

                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={this.props.minDate}
                    maximumDate={this.props.maxDate}
                    date={this.props.selectedDate}
                    mode={this.props.mode}
                    onConfirm={date=>this.handleDatePicked(date)}
                />
            </View>
        );
    };
}

export default CustomDatePickerView