import React, { Component } from 'react'
import { Platform, StyleSheet, View, Image, TextInput } from 'react-native';
import styles from './styles'
import Images from '../../constants/images'
import Constants from '../../constants/constants'

class CustomText extends Component {

    constructor() {
        super()
        this.state = {
            underLineColor: Constants.color.defaultUnderLineColor
        }
    }

    render() {
        return (
            <View style={[styles.container, this.props.container]}>
                <View style={styles.SectionStyle}>
                    <Image source={this.props.icon} style={styles.ImageStyle} />
                    <TextInput
                        style={{ flex: 1,fontSize:Constants.fontSize.SmallXXX }}
                        selectionColor={Constants.color.primary}
                        placeholder={this.props.placeholder}
                        placeholderTextColor={Constants.color.defaultTextColor}
                        underlineColorAndroid="transparent"
                        secureTextEntry={this.props.secureText}
                        onChangeText={text => this.props.onTextChange(text)}
                        onFocus={()=> this.setState({underLineColor : Constants.color.primary})}
                        onBlur={()=> this.setState({underLineColor : Constants.color.defaultUnderLineColor})}
                        value={this.props.value}
                    />
                </View>
                <View style={[styles.UnderlineStyle, { backgroundColor: this.state.underLineColor }]}></View>
            </View>
        );
    };
}

export default CustomText