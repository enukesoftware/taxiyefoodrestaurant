import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  DeviceEventEmitter,
  TouchableHighlight,
  FlatList
} from "react-native";

import Constants from '../../constants/constants'
export default class StatusModal extends Component {
  constructor(props) {
    super(props);
    this.state = {

      pendingStatusData: [
        {
          key: 7,
          value: "Confirmed"
        },
        {
          key: 2,
          value: "Open for Drivers"
        },
        // {
        //   key:3,
        //   value:"Accepted by Drivers"
        // },
        {
          key: 4,
          value: "Dispatched"
        },
        {
          key: 5,
          value: "Delivered"
        },

        {
          key: 6,
          value: "Cancelled"
        },


        //     {
        //       key:6,
        //       value:"Cancelled"
        //   },
      ],
      loading: false,
      selectedIndex: 0,

    };

    this._renderListItem = this._renderListItem.bind(this)
  }

  componentDidMount() {
    // console.log("CURRUNT",this.props.currentStatus)
    // console.log("STATUS_MODEL:")
    this.setDefaultStatus()
  }
  
  setDefaultStatus() {
    // console.log("CURRENT_STATUS:" + this.props.currentStatus)
    // console.log("CURRENT_STATUS_VALUE:" + this.getStatusObjectIndex(this.props.currentStatus))
    this.setState({ selectedIndex: this.getStatusObjectIndex(this.props.currentStatus) })
  }
  getStatusObjectIndex(currentStatus) {
    let indexValue = 0;
    this.state.pendingStatusData.map((item, index) => {
      if (item.key == currentStatus) {
        indexValue = index
      }
    })
    // console.log("RETURNSTATUS",s)
    return indexValue;
  }

  _renderListItem({ item, index }) {
    return (
      <TouchableHighlight
        underlayColor='transparent'
        onPress={() => {
          this.setState({
            selectedIndex: index
          })
        }}
      >
        <View style={styles.styleListItemView}>
          <View
            style={
              (this.state.selectedIndex) == index
                ? styles.styleListSelectedRadioBtn
                : styles.styleListradioUnSelect
            }
          />
          <Text style={[{ marginLeft: 20, fontSize: 15 }]}>
            {item.value}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }
  render() {
    return (
      // <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.statusModalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.container}>
          <View style={styles.stylePopUpView}>
            <View style={styles.styleTitleView}>
              <Text
                style={[{ fontSize: 18, }]}
              >Change Status
                </Text>
            </View>

            <FlatList
              style={{ marginLeft: 15, marginRight: 15 }}
              data={this.state.pendingStatusData}
              extraData={this.state}
              renderItem={this._renderListItem}
              keyExtractor={(item, index) => String(index)}
            />

            <View style={styles.styleBottomBtnsView}>

              <TouchableHighlight
                style={styles.styleCancelOrDoneBtnView}
                underlayColor="transparent"
                onPress={() => {
                  // DeviceEventEmitter.emit("StatusChanged", null);
                  this.setDefaultStatus()
                  this.props.closeModal();

                }}
              >
                <Text style={[{ fontSize: 14 }]}>
                  Cancel
                  </Text>
              </TouchableHighlight>

              <TouchableHighlight
                style={styles.styleCancelOrDoneBtnView}
                underlayColor="transparent"
                onPress={() => {
                  //alert("SelectedIndex: " + this.state.selectedIndex)
                  let data = this.state.pendingStatusData[this.state.selectedIndex];
                  this.props.closeModal();
                  this.props.okPressed(data)
                }}
              >
                {/* <Text style={{ fontSize:Constants.fontSize.SmallXX, fontFamily:Constants.fontFamily.Medium, }}>Done</Text> */}
                <Text style={[{ fontSize: 14 }]}
                >Done</Text>

              </TouchableHighlight>

            </View>
          </View>
        </View>
      </Modal>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: 'white',
    borderRadius: 15
  },
  styleTitleView: {
    margin: 15,
    height: 30,
    justifyContent: "center"
  },
  styleBottomBtnsView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 15,
    height: 35,
    paddingRight: 10
  },
  styleCancelOrDoneBtnView: {
    height: "100%",
    width: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  styleListItemView: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    alignItems: "center"
  },
  styleListSelectedRadioBtn: {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: Constants.color.primary
  },
  styleListradioUnSelect: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 3,
    borderColor: Constants.color.primary
  }
});