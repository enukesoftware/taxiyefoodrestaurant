import { StyleSheet } from 'react-native'
import Constants from './../../constants/constants'

export default StyleSheet.create({
    /*------------------ CustomText Start --------------*/

    container: {
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 0,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },

    ImageStyle: {
        padding: 5,
        margin: 5,
        height: 16,
        width: 16,
        resizeMode: 'stretch',
        alignItems: 'center',
        tintColor:Constants.color.primary
    },

    UnderlineStyle: {
        height: 1,
    },

    /*------------------ CustomText End --------------*/

    /*------------------ CustomDatePickerView Start --------------*/

    CustomDatePickerTextStyle:{
        flex:1,
        color:Constants.color.defaultTextColor,
        fontSize:Constants.fontSize.SmallXXX,
    },

    CustomDatePickerFirstImageStyle: {
        padding: 5,
        margin: 5,
        height: 16,
        width: 16,
        resizeMode: 'stretch',
        alignItems: 'center',
        tintColor:Constants.color.primary,
    },

    CustomDatePickerSecondImageStyle: {
        padding: 5,
        margin: 5,
        height: 12,
        width: 12,
        resizeMode: 'stretch',
        alignItems: 'center',
    },

    /*------------------ CustomDatePickerView End --------------*/
})