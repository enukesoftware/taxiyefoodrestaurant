import React, { Component } from "react"
import { Alert } from 'react-native'

export default class AlertManager extends Component {
    constructor(props) {
        super(props);
    }

    static showAlert(title, description,btnText) {
        Alert.alert(title,description,[{
            text:btnText
        }])
    }
}