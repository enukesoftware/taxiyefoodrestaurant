const Constants={
    color:{
        primary:'#2dca03',
        primaryDark:'#2dca03',
        blue:'#3f51b5',
        defaultUnderLineColor:'#CECFD0',
        defaultTextColor:'#3E3F40',
        progressDialogColor:'#000000',
        cardHeaderColor:'#D0F1D4',
        fieldNameColor:'#4C4E4C',
        fieldValueColor:'#212122',
        pageBackgroundColor:'#F4F5F6',
        yellow:'#FBDA8F',
        white:'#FFFFFF',
        lightPrimary:'#D0F1D4',
        lightGray:'#DCDCDC',
    },
    fontSize:{
        LargeXXX:26,
        LargeXX:24,
        LargeX:22,
        NormalXXX:20,
        NormalXX:18,
        NormalX:16,
        SmallXXX:14,
        SmallXX:12,
        SmallX:10,

    },
    fontFamily:{
        Regular:'Montserrat-Regular',
        // Medium:'Montserrat-Medium',
        Bold:'Montserrat-Bold',
        SemiBold:'Montserrat-SemiBold',
        Montserrat:{
            Regular:'Montserrat-Regular',
            // Medium:'Montserrat-Medium',
            Bold:'Montserrat-Bold',
            SemiBold:'Montserrat-SemiBold',
        }
       
    },
    url:{
        //baseUrl:'http://sfstaging.yiipro.com/en/api/v1/restaurant/',
       // baseUrl:'http://fd.yiipro.com/en/api/v1/restaurant/',
        baseUrl:'http://fd.yiipro.com/en/api/v1/restaurant/',
        signIn:'signin',
        orderList:'mobil-order-list',
        updateOrderList:'mobil-update-order-status',
        mobilSign:'mobil-signin',
        logout:'mobil-signout',
        forgotPassword:'mobil-forget-password',
        updateDeviceToken:'mobil-device-token-update',
    },

    login:{
        commonWidth:280,
    },

    currency:{
        dollar:'$ ',
        rupees:'₹ ',
    },
    ORDERS_STATUS:{
        NoConfirm:'0',
        Pending:'1',
        'Open for Drivers':'2',
        'Accepted by Driver':'3',
        Dispatched:'4',
        Delivered:'5',
        Cancelled:'6',
        Confirmed:'7',
    },
    ORDER_STATUS:{
        NoConfirm_0:'NoConfirm',
        Pending_1:'Pending',
        Drivers_2:'Drivers',
        Accepted_3:'Accepted',
        Dispatched_4:'Dispatched',
        Delivered_5:'Delivered',
        Cancelled_6:'Cancelled',
        Confirmed_7:'Confirmed',
    },
    EVENTS: {
        orderNotification: 'ORDER_NOTIFICATION',
        openOrderFromNotification: 'OPEN_ORDER_FROM_NOTIFICATION',
      },
    
}

export default Constants