const Images={
    userDefaultImage:require('../assets/img/user_default_image.png'),
    logoText:require('../assets/img/logo.png'),
    userName:require('../assets/img/ic_username.png'),
    password:require('../assets/img/ic_password.png'),
    calendar:require('../assets/img/ic_calendar.png'),
    dropdown:require('../assets/img/ic_dropdown.png'),
    orderId:require('../assets/img/ic_order_id.png'),
    status:require('../assets/img/ic_status.png'),
    location:require('../assets/img/ic_location.png'),
    mobile:require('../assets/img/ic_mobile.png'),
    back:require('../assets/img/ic_back.png'),
    search:require('../assets/img/ic_search.png'),
    mapMarker:require('../assets/img/icons8-map-pin-48.png'),
    logout:require('../assets/img/ic_logout.png'),
    call:require('../assets/img/ic_call.png'),
}


export default Images