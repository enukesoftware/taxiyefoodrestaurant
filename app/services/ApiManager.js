import React, { Component } from "react"
import Constants from './../constants/constants'

callApi = async (url, methodType, body) => {
    const header = {
        Accept: "application/json",
        "Content-Type": "application/json",
       // token: "abcd3142332",
        // agency_Id: Constants.agencyId
    };
    return fetch(
        url, {
            method: methodType,
            headers: header,
            body: methodType == "POST" ? JSON.stringify(body) : null
        }
    ).then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            // console.error(error);
            throw new Error(error)
        });
}

export const userSignIn = async (username,password) => {
    let body ={
        email:username,
        password:password
    }
    return callApi( Constants.url.baseUrl+Constants.url.mobilSign, "POST",body)
        .then((res) => {
           // console.log("resp...",res);
            return res;
        }).catch((error) => {
            console.log(error);
            throw new Error(error)
        });
}

export const orderList = async(body) => {
    // let body ={
    //     user_id:userId,
    //     from:fromDate,
    //     to:toDate,
    //     order_id:orderId,
    //     status:status,
    // }
   // console.log( from + to + orderId + status)
    return callApi( Constants.url.baseUrl+Constants.url.orderList, "POST",body)
    .then((res) => {
     //console.log("res....",res)
        return res;
    }).catch((error) => {
        console.log(error);
        throw new Error(error)
    });
}

export const updateOrderList = async(body) => {
    
    return callApi( Constants.url.baseUrl+Constants.url.updateOrderList, "POST",body)
    .then((res) => {
     console.log("res....",res)
        return res;
    }).catch((error) => {
        console.log(error);
        throw new Error(error)
    });
}
export const getDailyToken = async () => {
    let header = {
        Accept: "application/json",
        "Content-Type": "application/json"
    };

    return fetch(
        APIConstants.getFlightList, {
            method: "GET",
            headers: header,
            body: null
        })
        .then((response) => response.json())
        .then(async function (responseJson) {
            let token = responseJson.token
            console.log("got Token " + token);
            let now = new Date();
            let dict = {
                date: dateFormat(now, "yyyy-mm-dd"),
                token: token
            };
            Constants.dailyToken = token;
            console.log("saving dictionary is  " + JSON.stringify(dict));
            await this.saveJsonString("flightToken", JSON.stringify(dict));
            return responseJson.token
        })
        .catch((error) => { throw new Error(error) })

}

export const userLogout = async (userId) => {
    let body ={
        user_id:userId,
    }
    return callApi( Constants.url.baseUrl+Constants.url.logout, "POST",body)
        .then((res) => {
           // console.log("resp...",res);
            return res;
        }).catch((error) => {
            console.log(error);
            throw new Error(error)
        });
}

export const forgotPassword = async (email) => {
    let body ={
        email:email,
    }
    console.log("URL"+ Constants.url.baseUrl+Constants.url.forgotPassword)
    console.log("REQ:"+JSON.stringify(body))
    return callApi( Constants.url.baseUrl+Constants.url.forgotPassword, "POST",body)
        .then((res) => {
           // console.log("resp...",res);
            return res;
        }).catch((error) => {
            console.log(error);
            throw new Error(error)
        });
}

export const updateDeviceTokenApi = async (body) => {
    console.log("URL"+ Constants.url.baseUrl+Constants.url.updateDeviceToken)
    console.log("REQ:"+JSON.stringify(body))
    return callApi( Constants.url.baseUrl+Constants.url.updateDeviceToken, "POST",body)
        .then((res) => {
           // console.log("resp...",res);
            return res;
        }).catch((error) => {
            console.log(error);
            throw new Error(error)
        });
}