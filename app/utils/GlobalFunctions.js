import * as ApiManager from './../services/ApiManager'
import Constants from './../constants/constants'
import AsyncStorage from '@react-native-community/async-storage'


export async function callUpdateDeviceTokenApi() {
    console.log("CALL_UPDATE_TOKEN_API")

    const isLogin = await AsyncStorage.getItem('isLogin');
    const deviceToken = await AsyncStorage.getItem('fcmToken');
    const userDataAsString = await AsyncStorage.getItem('userData');
    const userData = JSON.parse(userDataAsString)


    if (!isLogin || !deviceToken || !userData || !userData.user_details || !userData.user_details.id) {
        return
    }
    
    const param = {
        id: userData.user_details.id,
        device_token: deviceToken,
    }

    ApiManager.updateDeviceTokenApi(param)
        .then(res => {
            console.log("UPDATE_TOKEN_RES:" + JSON.stringify(res))
            if (res.success) {

                // if (res.data != null) {
                //     AsyncStorage.setItem("isLogin", "true");
                //    console.log("USER_DATA:"+JSON.stringify(res.data)) 
                //    // let obj = JSON.parse(res.data);
                //   AsyncStorage.setItem('userData', JSON.stringify(res.data));
                //     this.props.navigation.navigate('SampleDashboard')
                // }
            } else {
                // setTimeout(() => {
                //     AlertManager.showAlert(res.message)
                // }, 500)
            }




            // console.warn(JSON.stringify(res))
        }).catch(err => {
            // setTimeout(() => {
            //     AlertManager.showAlert('Error', err, 'ok')
            // }, 500)
        })
}